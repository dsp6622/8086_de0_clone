
// Created by fizzim.pl version $Revision: 4.44 on 2014:10:16 at 14:27:11 (www.fizzim.com)


module PS2_Controller (
  inout PS2_CLK,
  inout PS2_DAT,
  output reg Err_Out,
  output reg Int_Out,
  output reg [7:0] Parallel_Out,
  output reg Ready_Out,
  input wire Ack_In,
  input wire [7:0] Parallel_In,
  input wire Req_In,
  input wire SYS_CLK,
  input wire nRESET
);

  // state bits
  parameter
  RESET         = 0,
  ERROR         = 1,
  IDLE          = 2,
  Rx1           = 3,
  Rx2           = 4,
  Rx3           = 5,
  Rx4           = 6,
  Rx5           = 7,
  RxParity      = 8,
  RxParityCheck = 9,
  RxStopBit1    = 10,
  RxStopBit2    = 11,
  Tx1           = 12,
  Tx2           = 13,
  Tx3           = 14,
  Tx4           = 15,
  Tx5           = 16,
  Tx6           = 17,
  Tx7           = 18,
  Tx8           = 19,
  Tx9           = 20,
  TxAck1        = 21,
  TxAck2        = 22,
  TxEnd         = 23,
  TxStop1       = 24,
  TxStop2       = 25;

  reg [25:0] state;
  reg [25:0] nextstate;
  reg PS2_clk_en;
  reg PS2_clk_in;
  reg PS2_clk_out;
  reg PS2_dat_en;
  reg PS2_dat_in;
  reg PS2_dat_out;
  reg [7:0] PS2_dat_shift;
  reg [3:0] bitCounter;
  reg parity;
  reg timeOut;
  reg [36:0] timeOutCounter;
  reg [13:0] timerCounter;
  reg timerDone;

  // comb always block
  always @* begin
    nextstate = 26'b00000000000000000000000000;
    timeOut = timeOutCounter == 1'b0; // default
    timerDone = timerCounter == 1'b0; // default
    case (1'b1) // synopsys parallel_case full_case
      state[RESET]        : begin
        timeOut = 0;
        timerDone = 0;
        begin
          nextstate[IDLE] = 1'b1;
        end
      end
      state[ERROR]        : begin
        if (Ack_In) begin
          nextstate[IDLE] = 1'b1;
        end
        else begin
          nextstate[ERROR] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[IDLE]         : begin
        timeOut = 0;
        timerDone = 0;
        if (Req_In) begin
          nextstate[Tx1] = 1'b1;
        end
        else if (PS2_clk_in == 1'b0) begin
          nextstate[Rx1] = 1'b1;
        end
        else begin
          nextstate[IDLE] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Rx1]          : begin
        if (timeOut | PS2_dat_in) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (PS2_clk_in) begin
          nextstate[Rx2] = 1'b1;
        end
        else begin
          nextstate[Rx1] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Rx2]          : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (!PS2_clk_in) begin
          nextstate[Rx3] = 1'b1;
        end
        else begin
          nextstate[Rx2] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Rx3]          : begin
        begin
          nextstate[Rx4] = 1'b1;
        end
      end
      state[Rx4]          : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (PS2_clk_in) begin
          nextstate[Rx5] = 1'b1;
        end
        else begin
          nextstate[Rx4] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Rx5]          : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (PS2_clk_in == 1'b0 && bitCounter == 4'd8) begin
          nextstate[RxParity] = 1'b1;
        end
        else if (!PS2_clk_in) begin
          nextstate[Rx3] = 1'b1;
        end
        else begin
          nextstate[Rx5] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[RxParity]     : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else begin
          nextstate[RxParityCheck] = 1'b1;
        end
      end
      state[RxParityCheck]: begin
        if (timeOut | Err_Out) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (PS2_clk_in) begin
          nextstate[RxStopBit1] = 1'b1;
        end
        else begin
          nextstate[RxParityCheck] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[RxStopBit1]   : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (!PS2_clk_in) begin
          nextstate[RxStopBit2] = 1'b1;
        end
        else begin
          nextstate[RxStopBit1] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[RxStopBit2]   : begin
        if (timeOut | !PS2_dat_in) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (Ack_In) begin
          nextstate[IDLE] = 1'b1;
        end
        else begin
          nextstate[RxStopBit2] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Tx1]          : begin
        begin
          nextstate[Tx2] = 1'b1;
        end
      end
      state[Tx2]          : begin
        if (timerDone) begin
          nextstate[Tx3] = 1'b1;
        end
        else begin
          nextstate[Tx2] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Tx3]          : begin
        begin
          nextstate[Tx4] = 1'b1;
        end
      end
      state[Tx4]          : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (timerDone) begin
          nextstate[Tx5] = 1'b1;
        end
        else begin
          nextstate[Tx4] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Tx5]          : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (!PS2_clk_in) begin
          nextstate[Tx6] = 1'b1;
        end
        else begin
          nextstate[Tx5] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Tx6]          : begin
        begin
          nextstate[Tx7] = 1'b1;
        end
      end
      state[Tx7]          : begin
        begin
          nextstate[Tx8] = 1'b1;
        end
      end
      state[Tx8]          : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (PS2_clk_in && bitCounter == 4'b9) begin
          nextstate[TxStop1] = 1'b1;
        end
        else if (PS2_clk_in) begin
          nextstate[Tx9] = 1'b1;
        end
        else begin
          nextstate[Tx8] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[Tx9]          : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (!PS2_clk_in) begin
          nextstate[Tx6] = 1'b1;
        end
        else begin
          nextstate[Tx9] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[TxAck1]       : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (!PS2_clk_in) begin
          nextstate[TxAck2] = 1'b1;
        end
        else begin
          nextstate[TxAck1] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[TxAck2]       : begin
        if (PS2_dat_in) begin
          nextstate[ERROR] = 1'b1;
        end
        else begin
          nextstate[TxEnd] = 1'b1;
        end
      end
      state[TxEnd]        : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (PS2_clk_in && PS2_dat_in) begin
          nextstate[IDLE] = 1'b1;
        end
        else begin
          nextstate[TxEnd] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[TxStop1]      : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (!PS2_clk_in) begin
          nextstate[TxStop2] = 1'b1;
        end
        else begin
          nextstate[TxStop1] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[TxStop2]      : begin
        if (timeOut) begin
          nextstate[ERROR] = 1'b1;
        end
        else if (PS2_clk_in) begin
          nextstate[TxAck1] = 1'b1;
        end
        else begin
          nextstate[TxStop2] = 1'b1; // Added because implied_loopback is true
        end
      end
    endcase
  end

  // sequential always block
  always @(posedge SYS_CLK or negedge nRESET) begin
    if (!nRESET)
      state <= 26'b00000000000000000000000001 << RESET;
    else
      state <= nextstate;
  end

  // datapath sequential always block
  always @(posedge SYS_CLK or negedge nRESET) begin
    if (!nRESET) begin
      Err_Out <= 0;
      Int_Out <= 0;
      PS2_clk_en <= PS2_clk_en;
      PS2_clk_in <= 1'b1;
      PS2_clk_out <= PS2_clk_out;
      PS2_dat_en <= PS2_dat_en;
      PS2_dat_in <= 1'b1;
      PS2_dat_out <= PS2_dat_out;
      PS2_dat_shift[7:0] <= 8'd0;
      Parallel_Out[7:0] <= 8'd0;
      Ready_Out <= 0;
      bitCounter[3:0] <= 4'd0;
      parity <= 0;
      timeOutCounter[36:0] <= 37'd1500000;
      timerCounter[13:0] <= 14'd0;
    end
    else begin
      Err_Out <= Err_Out; // default
      Int_Out <= Int_Out; // default
      PS2_clk_en <= PS2_clk_en; // default
      PS2_clk_in <= (PS2_clk_en != 1'b1) ? PS2_CLK : PS2_clk_in; // default
      PS2_clk_out <= PS2_clk_out; // default
      PS2_dat_en <= PS2_dat_en; // default
      PS2_dat_in <= (PS2_dat_en != 1'b1) ? PS2_DAT : PS2_dat_in; // default
      PS2_dat_out <= PS2_dat_out; // default
      PS2_dat_shift[7:0] <= PS2_dat_shift; // default
      Parallel_Out[7:0] <= Parallel_Out; // default
      Ready_Out <= 0; // default
      bitCounter[3:0] <= bitCounter; // default
      parity <= parity; // default
      timeOutCounter[36:0] <= timeOutCounter - 1'b1; // default
      timerCounter[13:0] <= timerCounter - 1'b1; // default
      case (1'b1) // synopsys parallel_case full_case
        nextstate[RESET]        : begin
          Err_Out <= 0;
          Int_Out <= 0;
          PS2_clk_in <= 1;
          PS2_dat_in <= 1;
          PS2_dat_shift[7:0] <= 8'd0;
          Parallel_Out[7:0] <= 8'd0;
          bitCounter[3:0] <= 4'd0;
          parity <= 0;
          timeOutCounter[36:0] <= 37'd1500000;
          timerCounter[13:0] <= 14'd0;
        end
        nextstate[ERROR]        : begin
          Int_Out <= 1;
        end
        nextstate[IDLE]         : begin
          Int_Out <= 0;
          PS2_clk_en <= 0;
          PS2_clk_out <= 1;
          PS2_dat_en <= 0;
          PS2_dat_out <= 1;
          PS2_dat_shift[7:0] <= 8'd0;
          Ready_Out <= 1;
          bitCounter[3:0] <= 4'd0;
          timeOutCounter[36:0] <= 37'd1500000;
        end
        nextstate[Rx1]          : begin
          ; // case must be complete for onehot
        end
        nextstate[Rx2]          : begin
          ; // case must be complete for onehot
        end
        nextstate[Rx3]          : begin
          PS2_dat_shift[7:0] <= {PS2_dat_shift[6:0],PS2_dat_in};
          bitCounter[3:0] <= bitCounter + 1'b1;
        end
        nextstate[Rx4]          : begin
          ; // case must be complete for onehot
        end
        nextstate[Rx5]          : begin
          ; // case must be complete for onehot
        end
        nextstate[RxParity]     : begin
          Err_Out <= !(^{Parallel_Out,parity});
          Parallel_Out[7:0] <= PS2_dat_shift;
          parity <= PS2_dat_in;
        end
        nextstate[RxParityCheck]: begin
          ; // case must be complete for onehot
        end
        nextstate[RxStopBit1]   : begin
          ; // case must be complete for onehot
        end
        nextstate[RxStopBit2]   : begin
          Int_Out <= 1;
        end
        nextstate[Tx1]          : begin
          PS2_clk_en <= 1'b1;
          PS2_clk_out <= 1'b0;
          PS2_dat_shift[7:0] <= Parallel_In;
          bitCounter[3:0] <= 4'd0;
          parity <= !(^Parallel_In);
          timerCounter[13:0] <= 14'd10000;
        end
        nextstate[Tx2]          : begin
          ; // case must be complete for onehot
        end
        nextstate[Tx3]          : begin
          PS2_dat_en <= 1'b1;
          PS2_dat_out <= 1'b0;
          timerCounter[13:0] <= 14'd100;
        end
        nextstate[Tx4]          : begin
          ; // case must be complete for onehot
        end
        nextstate[Tx5]          : begin
          PS2_clk_en <= 1'b0;
          PS2_clk_out <= 1'b1;
        end
        nextstate[Tx6]          : begin
          PS2_dat_out <= PS2_dat_shift[0];
          bitCounter[3:0] <= bitCounter + 1'b1;
        end
        nextstate[Tx7]          : begin
          PS2_dat_shift[7:0] <= {parity, PS2_dat_shift[7:1]};
        end
        nextstate[Tx8]          : begin
          ; // case must be complete for onehot
        end
        nextstate[Tx9]          : begin
          ; // case must be complete for onehot
        end
        nextstate[TxAck1]       : begin
          ; // case must be complete for onehot
        end
        nextstate[TxAck2]       : begin
          ; // case must be complete for onehot
        end
        nextstate[TxEnd]        : begin
          ; // case must be complete for onehot
        end
        nextstate[TxStop1]      : begin
          ; // case must be complete for onehot
        end
        nextstate[TxStop2]      : begin
          PS2_dat_en <= 1'b0;
          PS2_dat_out <= 1'b1;
        end
      endcase
    end
  end

  // This code allows you to see state names in simulation
  `ifndef SYNTHESIS
  reg [103:0] statename;
  always @* begin
    case (1'b1)
      state[RESET]        :
        statename = "RESET";
      state[ERROR]        :
        statename = "ERROR";
      state[IDLE]         :
        statename = "IDLE";
      state[Rx1]          :
        statename = "Rx1";
      state[Rx2]          :
        statename = "Rx2";
      state[Rx3]          :
        statename = "Rx3";
      state[Rx4]          :
        statename = "Rx4";
      state[Rx5]          :
        statename = "Rx5";
      state[RxParity]     :
        statename = "RxParity";
      state[RxParityCheck]:
        statename = "RxParityCheck";
      state[RxStopBit1]   :
        statename = "RxStopBit1";
      state[RxStopBit2]   :
        statename = "RxStopBit2";
      state[Tx1]          :
        statename = "Tx1";
      state[Tx2]          :
        statename = "Tx2";
      state[Tx3]          :
        statename = "Tx3";
      state[Tx4]          :
        statename = "Tx4";
      state[Tx5]          :
        statename = "Tx5";
      state[Tx6]          :
        statename = "Tx6";
      state[Tx7]          :
        statename = "Tx7";
      state[Tx8]          :
        statename = "Tx8";
      state[Tx9]          :
        statename = "Tx9";
      state[TxAck1]       :
        statename = "TxAck1";
      state[TxAck2]       :
        statename = "TxAck2";
      state[TxEnd]        :
        statename = "TxEnd";
      state[TxStop1]      :
        statename = "TxStop1";
      state[TxStop2]      :
        statename = "TxStop2";
      default      :
        statename = "XXXXXXXXXXXXX";
    endcase
  end
  `endif

  // Inserted from attribute insert_at_bottom_of_module:
  assign PS2_DAT = (PS2_dat_en != 1'b0) ? PS2_dat_out : 1'bZ;
  assign PS2_CLK = (PS2_clk_en != 1'b0) ? PS2_clk_out : 1'bZ;

endmodule
