onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /SDRAM/initCounter
add wave -noupdate /SDRAM/SDCLK
add wave -noupdate /SDRAM/state
add wave -noupdate /SDRAM/refreshCounter
add wave -noupdate /SDRAM/DataIn
add wave -noupdate /SDRAM/AddrIn
add wave -noupdate /SDRAM/WE
add wave -noupdate /SDRAM/Start
add wave -noupdate /SDRAM/niLDQM
add wave -noupdate /SDRAM/niUDQM
add wave -noupdate /SDRAM/intLDQM
add wave -noupdate /SDRAM/intUDQM
add wave -noupdate /SDRAM/Ack
add wave -noupdate /SDRAM/DataOut
add wave -noupdate /SDRAM/nReset
add wave -noupdate /SDRAM/DQi
add wave -noupdate /SDRAM/DQ
add wave -noupdate /SDRAM/ADDRE
add wave -noupdate /SDRAM/BA
add wave -noupdate /SDRAM/n_RAS
add wave -noupdate /SDRAM/n_CAS
add wave -noupdate /SDRAM/nWriteEN
add wave -noupdate /SDRAM/nLDQM
add wave -noupdate /SDRAM/nUDQM
add wave -noupdate /SDRAM/addressOut
add wave -noupdate /SDRAM/internalAddress
add wave -noupdate /SDRAM/internalData
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {214959 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {12746 ns} {76746 ns}
