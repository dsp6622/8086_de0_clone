#!/bin/bash

# This script sets up a docker container that runs modelsim. It depends on a file called altera.tar, which contains the altera-modelsim library files.
# I can't distribute those files unfortunately, feel free to contact me if you need help setting it up, or alternatively just install modelsim altera edition
# however you normally would.
# Do files are located in src/modelsim, if compilation fails try refreshing the cyclone iii library


SHARE_SOURCE=$1
if [ -z $1 ]; then
    echo Specify what directory the repo is in
    exit -1
fi
SHARE_DEST=/8086repo/
XAUTH_ARGS=$(xauth list | grep "unix${DISPLAY} ");
DOCKER_CREATE_EXTRAS=" -v ${SHARE_SOURCE}:${SHARE_DEST} "
DOCKER_CREATE_COMMAND="docker create -i -t --name modelsim --net=host -e DISPLAY -v /tmp/.X11-unix ${DOCKER_CREATE_EXTRAS} goldensniper/modelsim-docker"
if ! docker container list -a | grep -i "goldensniper/modelsim-docker"; then
    $DOCKER_CREATE_COMMAND bash -c "if [ ! -d /vsim-ase/altera ]; then cd /vsim-ase && tar -xvf /8086repo/altera.tar; fi && touch /root/.Xauthority && xauth add ${XAUTH_ARGS} && vsim"
fi

docker start modelsim
