`define BIT_COUNTER_SIZE 3
`define CPHA 0
`define CPOL 0
`define FRAME_DELAY 2
`define SS_DELAY 3
`define TIMER_COUNTER_SIZE 3
`define WORD_SIZE 8

module SPI(
  output reg MOSI,
  output reg [`WORD_SIZE:0] Parallel_Out,
  output reg Ready,
  output reg SPI_Clk_Out,
  output reg SS,
  input wire MISO,
  input wire [`WORD_SIZE:0] Parallel_In,
  input wire SPI_Double_Clk,
  input wire Start_Transaction,
  input wire nReset
);

  // state bits
  parameter
  IDLE          = 0,
  DELAY_FRAME   = 1,
  DELAY_SS      = 2,
  START         = 3,
  TRANSFER1     = 4,
  TRANSFER2     = 5,
  TRANSFER_DONE = 6;

  reg [6:0] state;
  reg [6:0] nextstate;
  reg [`BIT_COUNTER_SIZE:0] bitCounter;
  reg [`WORD_SIZE:0] shift_reg;
  reg [`TIMER_COUNTER_SIZE:0] timerCounter;
  reg timerDone;

  // comb always block
  always @* begin
    nextstate = 7'b0000000;
    timerDone = timerCounter == 1'b0; // default
    case (1'b1) // synopsys parallel_case full_case
      state[IDLE]         : begin
        if (Start_Transaction) begin
          nextstate[START] = 1'b1;
        end
        else begin
          nextstate[IDLE] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[DELAY_FRAME]  : begin
        if (timerDone) begin
          nextstate[TRANSFER1] = 1'b1;
        end
        else begin
          nextstate[DELAY_FRAME] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[DELAY_SS]     : begin
        if (timerDone) begin
          nextstate[TRANSFER1] = 1'b1;
        end
        else begin
          nextstate[DELAY_SS] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[START]        : begin
        begin
          nextstate[DELAY_SS] = 1'b1;
        end
      end
      state[TRANSFER1]    : begin
        begin
          nextstate[TRANSFER2] = 1'b1;
        end
      end
      state[TRANSFER2]    : begin
        if (bitCounter == `WORD_SIZE) begin
          nextstate[TRANSFER_DONE] = 1'b1;
        end
        else begin
          nextstate[TRANSFER1] = 1'b1;
        end
      end
      state[TRANSFER_DONE]: begin
        if (Start_Transaction) begin
          nextstate[DELAY_FRAME] = 1'b1;
        end
        else begin
          nextstate[IDLE] = 1'b1;
        end
      end
    endcase
  end

  // sequential always block
  always @(posedge SPI_Double_Clk or negedge nReset) begin
    if (!nReset)
      state <= 7'b0000001 << IDLE;
    else
      state <= nextstate;
  end

  // datapath sequential always block
  always @(posedge SPI_Double_Clk or negedge nReset) begin
    if (!nReset) begin
      MOSI <= 1'b1;
      Parallel_Out[`WORD_SIZE:0] <= 0;
      Ready <= 1'b0;
      SPI_Clk_Out <= `CPOL;
      SS <= 1'b1;
      bitCounter[`BIT_COUNTER_SIZE:0] <= 0;
      shift_reg[`WORD_SIZE:0] <= shift_reg;
      timerCounter[`TIMER_COUNTER_SIZE:0] <= 0;
    end
    else begin
      MOSI <= MOSI; // default
      Parallel_Out[`WORD_SIZE:0] <= Parallel_Out; // default
      Ready <= 1'b0; // default
      SPI_Clk_Out <= SPI_Clk_Out; // default
      SS <= SS; // default
      bitCounter[`BIT_COUNTER_SIZE:0] <= bitCounter; // default
      shift_reg[`WORD_SIZE:0] <= shift_reg; // default
      timerCounter[`TIMER_COUNTER_SIZE:0] <= timerCounter - 1'b1; // default
      case (1'b1) // synopsys parallel_case full_case
        nextstate[IDLE]         : begin
          SPI_Clk_Out <= `CPOL;
          SS <= 1'b1;
        end
        nextstate[DELAY_FRAME]  : begin
          shift_reg[`WORD_SIZE:0] <= Parallel_In;
        end
        nextstate[DELAY_SS]     : begin
          ; // case must be complete for onehot
        end
        nextstate[START]        : begin
          Ready <= 1'b1;
          SS <= 1'b0;
          shift_reg[`WORD_SIZE:0] <= Parallel_In;
          timerCounter[`TIMER_COUNTER_SIZE:0] <= `SS_DELAY;
        end
        nextstate[TRANSFER1]    : begin
          MOSI <= (`CPHA) ? MOSI : shift_reg[`WORD_SIZE-1];
          SPI_Clk_Out <= !`CPOL;
          bitCounter[`BIT_COUNTER_SIZE:0] <= (`CPHA) ? bitCounter + 1'b1 : bitCounter;
          shift_reg[`WORD_SIZE:0] <= (`CPHA) ? {shift_reg[`WORD_SIZE-2:0], MISO} : shift_reg;

        end
        nextstate[TRANSFER2]    : begin
          MOSI <= MOSI <= (!`CPHA) ? MOSI : shift_reg[`WORD_SIZE-1];
          SPI_Clk_Out <= `CPOL;
          bitCounter[`BIT_COUNTER_SIZE:0] <= bitCounter <= (!`CPHA) ? bitCounter + 1'b1 : bitCounter;
          shift_reg[`WORD_SIZE:0] <= (!`CPHA) ? {shift_reg[`WORD_SIZE-2:0], MISO} : shift_reg;
        end
        nextstate[TRANSFER_DONE]: begin
          Parallel_Out[`WORD_SIZE:0] <= shift_reg;
          Ready <= 1'b1;
          bitCounter[`BIT_COUNTER_SIZE:0] <= 0;
          timerCounter[`TIMER_COUNTER_SIZE:0] <= `FRAME_DELAY;
        end
      endcase
    end
  end

  // This code allows you to see state names in simulation
  `ifndef SYNTHESIS
  reg [103:0] statename;
  always @* begin
    case (1'b1)
      state[IDLE]         :
        statename = "IDLE";
      state[DELAY_FRAME]  :
        statename = "DELAY_FRAME";
      state[DELAY_SS]     :
        statename = "DELAY_SS";
      state[START]        :
        statename = "START";
      state[TRANSFER1]    :
        statename = "TRANSFER1";
      state[TRANSFER2]    :
        statename = "TRANSFER2";
      state[TRANSFER_DONE]:
        statename = "TRANSFER_DONE";
      default      :
        statename = "XXXXXXXXXXXXX";
    endcase
  end
  `endif

endmodule
`undef BIT_COUNTER_SIZE 
`undef CPHA 
`undef CPOL 
`undef FRAME_DELAY 
`undef SS_DELAY 
`undef TIMER_COUNTER_SIZE 
`undef WORD_SIZE 
