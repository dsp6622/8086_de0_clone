www.davepearlman.com
 
This design is meant to be loaded onto a terrasic DE0 development board. Any other 
board will require different pin placements. The code itself 
should be portable enough to load onto another device, other than the altera
megafunctions which will likely need to be re-created. 

This project was started to meet a goal that I had when I was young. I wanted
to know computers well enough to be able to design one from scratch. The 
PC-XT was not a good platform choice for education, as the 8086 has varriable
length instructions, convoluted segmentation, and complex instructions. I
chose it for nostalgic reasons. Commander Keen 4 was the first game I remember
playing, on an IBM PS/2 system. I was born a year or two after that computer was
manufactured, and I still have it in working condition. I am not aiming for
complete compatibility or implementation, so this project will probably never be
of use to anyone. If I ever decide to refactor my code into a nicer state, maybe
it could serve as some educational reference HDL, but I am not aiming for speed,
conformity, or modularity. I welcome any constructive criticism to help me
improve my coding. Keep in mind that as of this commit, much of the code is
around three years old, and I have not yet graduated college. Anything from here
on out, I have no excuse for. =D
