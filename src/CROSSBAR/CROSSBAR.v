//synopsys translate on
// Address and mask lines are fanned out, WE, Ack, Read Data, and Start are multiplexed based on mapping
// This is for the weird YAMI interface I made a while back, guess I might as well stick with it now.
// If anyone cares, my opinion is that ready and valid signals are less error prone than Ack pulses.
// That would allow the master to do other things after writing instead of waiting for the Ack, since
// any future reads or writes would check the ready line. 

module CROSSBAR(
  // Crossbar master interface
  input  wire [19:0] Address,
  input  wire [15:0] DataIn,
  input  wire WriteEnable,
  input  wire UpperMask,
  input  wire LowerMask,
  input  wire StartTransaction,
  output reg  [15:0] DataOut,
  output reg  Ack,

  // VRAM interface
  output wire [14:0] VideoAddress,
  output wire [15:0] VideoWriteData,
  output reg  VideoWE,
  output reg  VideoStart,
  output wire VideoUM,
  output wire VideoLM,
  input  wire [15:0] VideoReadData,
  input  wire VideoAck,
  // SDRAM interface
  output wire [19:0] RamAddress,
  output wire [15:0] RamWriteData,
  output reg  RamWE,
  output reg  RamStart,
  output wire RamUM,
  output wire RamLM,
  input  wire [15:0] RamReadData,
  input  wire RamAck,
  // Bios ROM interface
  output wire [12:0] BiosAddress,
  output wire [15:0] BiosWriteData,
  output reg  BiosWE,
  output reg  BiosStart,
  output wire BiosUM,
  output wire BiosLM,
  input  wire [15:0] BiosReadData,
  input  wire BiosAck

);

`define CGA_ADDRESS_MASK 20'b10111xxxxxxxxxxxxxxx

// Right now the BIOS memory will alias since it doesn't
// take the whole allocated range
`define BIOS_ADDRESS_MASK 20'b1111xxxxxxxxxxxxxxxx

assign VideoAddress = {Address[14:1], 1'b0};
assign VideoWriteData = DataIn;
assign VideoUM = !UpperMask;
assign VideoLM = !LowerMask;

// Bios rom was configured to have 16-bit words, so the least significant
// byte isn't needed
assign BiosAddress = {Address[13:1]};
assign BiosWriteData = DataIn;
assign BiosUM = !UpperMask;
assign BiosLM = !LowerMask;

assign RamAddress = {Address[19:1], 1'b0};
assign RamWriteData = DataIn;
assign RamUM = UpperMask;
assign RamLM = LowerMask;

always@(*) begin

  //Defaults
  VideoWE <= 1'b0;
  VideoStart <= 1'b0;
  BiosWE <= 1'b0;
  BiosStart <= 1'b0;
  RamWE <= 1'b0;
  RamStart <= 1'b0;

  casex(Address)
    `CGA_ADDRESS_MASK: begin
      VideoWE <= WriteEnable;
      VideoStart <= StartTransaction;
      DataOut <= VideoReadData;
      Ack <= VideoAck;
    end

    `BIOS_ADDRESS_MASK: begin
      BiosWE <= WriteEnable;
      BiosStart <= StartTransaction;
      DataOut <= BiosReadData;
      Ack <= BiosAck;
	 end

    default: begin
      RamWE <= WriteEnable;
      RamStart <= StartTransaction;
      DataOut <= RamReadData;
      Ack <= RamAck;
	 end

  endcase

end

endmodule
