
// Created by fizzim.pl version $Revision: 4.44 on 2014:11:12 at 17:54:03 (www.fizzim.com)

module SPI (
  output reg MOSI,
  output reg [8:0] Parallel_Out,
  output reg Ready,
  output reg SPI_Clk_Out,
  output reg SS, //Slave Select
  input wire MISO,
  input wire [8:0] Parallel_In,
  input wire SPI_Double_Clk,
  input wire Start_Transaction,
  input wire nReset
);

  // state bits
  parameter
  IDLE          = 0,
  DELAY_FRAME   = 1,
  DELAY_SS      = 2,
  START         = 3,
  TRANSFER1     = 4,
  TRANSFER1_5   = 5,
  TRANSFER2     = 6,
  TRANSFER2_5   = 7,
  TRANSFER_DONE = 8;

  reg [8:0] state;
  reg [8:0] nextstate;
  reg [3:0] bitCounter;
  reg [8:0] shift_reg;
  reg [3:0] timerCounter;
  reg timerDone;

  // comb always block
  always @* begin
    nextstate = 9'b000000000;
    timerDone = timerCounter == 1'b0; // default
    case (1'b1) // synopsys parallel_case full_case
      state[IDLE]         : begin
        if (Start_Transaction) begin
          nextstate[START] = 1'b1;
        end
        else begin
          nextstate[IDLE] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[DELAY_FRAME]  : begin
        if (timerDone) begin
          nextstate[TRANSFER1] = 1'b1;
        end
        else begin
          nextstate[DELAY_FRAME] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[DELAY_SS]     : begin
        if (timerDone) begin
          nextstate[TRANSFER1] = 1'b1;
        end
        else begin
          nextstate[DELAY_SS] = 1'b1; // Added because implied_loopback is true
        end
      end
      state[START]        : begin
        begin
          nextstate[DELAY_SS] = 1'b1;
        end
      end
      state[TRANSFER1]    : begin
        begin
          nextstate[TRANSFER1_5] = 1'b1;
        end
      end
      state[TRANSFER1_5]  : begin
        begin
          nextstate[TRANSFER2] = 1'b1;
        end
      end
      state[TRANSFER2]    : begin
        begin
          nextstate[TRANSFER2_5] = 1'b1;
        end
      end
      state[TRANSFER2_5]  : begin
        if (bitCounter == 4'd8) begin
          nextstate[TRANSFER_DONE] = 1'b1;
        end
        else begin
          nextstate[TRANSFER1] = 1'b1;
        end
      end
      state[TRANSFER_DONE]: begin
        if (Start_Transaction) begin
          nextstate[DELAY_FRAME] = 1'b1;
        end
        else begin
          nextstate[IDLE] = 1'b1;
        end
      end
    endcase
  end

  // sequential always block
  always @(posedge SPI_Double_Clk or negedge nReset) begin
    if (!nReset)
      state <= 9'b000000001 << IDLE;
    else
      state <= nextstate;
  end

  // datapath sequential always block
  always @(posedge SPI_Double_Clk or negedge nReset) begin
    if (!nReset) begin
      MOSI <= 1'b1;
      Parallel_Out[8:0] <= 8'd0;
      Ready <= 1'b1;
      SPI_Clk_Out <= 0;
      SS <= 1'b1;
      bitCounter[3:0] <= 3'd0;
      shift_reg[8:0] <= shift_reg;
      timerCounter[3:0] <= 3'd0;
    end
    else begin
      MOSI <= MOSI; // default
      Parallel_Out[8:0] <= Parallel_Out; // default
      Ready <= 1'b0; // default
      SPI_Clk_Out <= SPI_Clk_Out; // default
      SS <= SS; // default
      bitCounter[3:0] <= bitCounter; // default
      shift_reg[8:0] <= shift_reg; // default
      timerCounter[3:0] <= timerCounter - 1'b1; // default
      case (1'b1) // synopsys parallel_case full_case
        nextstate[IDLE]         : begin
          Ready <= 1'b1;
          SPI_Clk_Out <= 0;
          SS <= 1'b1;
        end
        nextstate[DELAY_FRAME]  : begin
          shift_reg[8:0] <= Parallel_In;
        end
        nextstate[DELAY_SS]     : begin
        end
        nextstate[START]        : begin
          SS <= 1'b0;
          shift_reg[8:0] <= Parallel_In;
          timerCounter[3:0] <= 2;
        end
        nextstate[TRANSFER1]    : begin
          MOSI <= shift_reg[7];
        end
        nextstate[TRANSFER1_5]  : begin
          SPI_Clk_Out <= 1'b1;
        end
        nextstate[TRANSFER2]    : begin
          bitCounter[3:0] <= bitCounter + 1'b1;
          shift_reg[8:0] <= {shift_reg[6:0], MISO} ;
        end
        nextstate[TRANSFER2_5]  : begin
          SPI_Clk_Out <= 1'b0;
        end
        nextstate[TRANSFER_DONE]: begin
          Parallel_Out[8:0] <= shift_reg;
          Ready <= 1'b1;
          bitCounter[3:0] <= 3'd0;
          timerCounter[3:0] <= 3;
        end
      endcase
    end
  end

  // This code allows you to see state names in simulation
  `ifndef SYNTHESIS
  reg [103:0] statename;
  always @* begin
    case (1'b1)
      state[IDLE]         :
        statename = "IDLE";
      state[DELAY_FRAME]  :
        statename = "DELAY_FRAME";
      state[DELAY_SS]     :
        statename = "DELAY_SS";
      state[START]        :
        statename = "START";
      state[TRANSFER1]    :
        statename = "TRANSFER1";
      state[TRANSFER1_5]  :
        statename = "TRANSFER1_5";
      state[TRANSFER2]    :
        statename = "TRANSFER2";
      state[TRANSFER2_5]  :
        statename = "TRANSFER2_5";
      state[TRANSFER_DONE]:
        statename = "TRANSFER_DONE";
      default      :
        statename = "XXXXXXXXXXXXX";
    endcase
  end
  `endif

endmodule
