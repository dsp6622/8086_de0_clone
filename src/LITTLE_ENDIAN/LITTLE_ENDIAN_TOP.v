//synopsys translate on
module LITTLE_ENDIAN(
  
  input wire SysClk,
  input wire nReset,
  // Slave side
  input wire CPU_Start,
  input wire [19:0] sAddress,
  input wire [15:0] sDataIn,
  input wire sWriteEnable,
  input wire sWordByte,
  
  output reg [15:0] sDataOut,
  output reg sAck,

  // Master side
  output reg [19:0] mAddress,
  output wire [15:0] mDataOut,
  output reg mWriteEnable,
  output reg mUpperMask,
  output reg mLowerMask, //1 masks, 0 passes
  output reg mStartTransaction,

  input wire [15:0] mDataIn,
  input wire mAck
  
);

reg readSwap;
reg writeSwap;
wire [15:0] mDataOut_int;
wire [15:0] sDataOut_int;
wire A0;
assign A0 = sAddress[0];

BYTE_SWAP read_swap(
  .Swap(readSwap),
  .DataIn(mDataIn),
  .DataOut (sDataOut_int)
);
BYTE_SWAP write_swap(
  .Swap(writeSwap),
  .DataIn(sDataIn),
  .DataOut (mDataOut_int)
);
`define NUM_ENDIAN_STATES 15
parameter IDLE = 0,
          REa   = 1,
          REb   = 2,
          RBOa  = 3,
          RBOb  = 4,
          RWOa  = 5,
          RWOb  = 6,
          RWOc  = 7,
          RWOd  = 8,
          WB    = 9,
          WWE   = 10,
          WWOa  = 11,
          WWOb  = 12,
          WWOc  = 13,
          W_ACK = 14;


reg [`NUM_ENDIAN_STATES - 1:0] state, next /* synthesis preserve */ ;
always @(posedge SysClk, negedge nReset) begin
  if (!nReset) begin
    state <= `NUM_ENDIAN_STATES'b0;
    state[IDLE] <= 1'b1;
  end
  else begin 
    state <= `NUM_ENDIAN_STATES'b0;
    state <= next;
  end
end

always @(*) begin
  next <= `NUM_ENDIAN_STATES'b0;
  case (1'b1) // synopsys parallel_case
    state[IDLE] : begin
      if(CPU_Start == 1'b1) begin
        case({sWriteEnable, sWordByte, A0})
        3'b000: //Read a byte to an even address
          next[REa] <= 1'b1;
        3'b001: //Read a byte to an odd address
          next[RBOa] <= 1'b1;
        3'b010: //Read a word to an even address
          next[REa] <= 1'b1;
        3'b011: //Read a word to an odd address
          next[RWOa] <= 1'b1;
        3'b100: //Write a byte to an even address
          next[WB] <= 1'b1;
        3'b101: //Write a byte to an odd address
          next[WB] <= 1'b1;
        3'b110: //Write a word to an even address
          next[WWE] <= 1'b1;
        3'b111: //Write a word to an odd address
          next[WWOa] <= 1'b1;
        default: begin
          next <= `NUM_ENDIAN_STATES'b0;
          next[IDLE] <= 1'b1;
        end
        endcase
      end else begin
        next[IDLE] <= 1'b1;
      end
    end

    state[REa]: begin
      next[REb] <= 1'b1;
    end

    state[REb]: begin
      if(mAck == 1'b1) begin
        next[IDLE] <= 1'b1;
      end else begin
        next[REb] <= 1'b1;
      end
    end

    state[RBOa]: begin
      next[RBOb] <= 1'b1;
    end
    
    state[RBOb]: begin
      if(mAck == 1'b1) begin
        next[IDLE] <= 1'b1;
      end else begin
        next[RBOb] <= 1'b1;
      end
    end

    state[RWOa]: begin
      next[RWOb] <= 1'b1;
    end

    state[RWOb]: begin
      if(mAck == 1'b1) begin
        next[RWOc] <= 1'b1;
      end else begin
        next[RWOb] <= 1'b1;
      end
    end

    state[RWOc]: begin
      next[RWOd] <= 1'b1;
    end

    state[RWOd]: begin
      if(mAck == 1'b1) begin
        next[IDLE] <= 1'b1;
      end else begin
        next[RWOd] <= 1'b1;
      end
    end   

    state[WB]: begin
      next[W_ACK] <= 1'b1;
    end
 
    state[WWE]: begin
      next[W_ACK] <= 1'b1;
    end   
    
    state[WWOa]: begin
      next[WWOb] <= 1'b1;
    end   
    
    state[WWOb]: begin
      if(mAck == 1'b1) begin
        next[WWOc] <= 1'b1;
      end else begin
        next[WWOb] <= 1'b1;
      end
    end

    state[WWOc]: begin
      next[W_ACK] <= 1'b1;
    end
    
    state[W_ACK]: begin
      if(mAck == 1'b1) begin
        next[IDLE] <= 1'b1;
      end else begin
        next[W_ACK] <= 1'b1;
      end
    end
    
    default : begin
      next[IDLE] = 1'b1;    
    end
 endcase
 end
 
 assign mDataOut = mDataOut_int; // Byte masks control which byte is written

 always@(posedge SysClk) begin
  //Defaults 
  mStartTransaction <= 1'b0;
  mWriteEnable <= 1'b0;
  sAck <= 1'b0;

  case (1'b1) // synopsys parallel_case
    state[IDLE]: begin 
      mAddress <= {sAddress[19:1], 1'b0};
      mUpperMask <= 1'b0;
      mLowerMask <= 1'b0;
      sAck <= 1'b0;
      readSwap <= 1'b0;
      writeSwap <= 1'b0;
    end

    state[REa]: begin
      readSwap <= 1'b1;
      mStartTransaction <= 1'b1;
    end

    state[REb]: begin
      sDataOut <= sDataOut_int;
      sAck <= mAck;
    end
    
    state[RBOa]: begin
      readSwap <= 1'b0;
      mStartTransaction <= 1'b1;
    end

    state[RBOb]: begin
      sDataOut <= sDataOut_int;
      sAck <= mAck;
    end

    state[RWOa]: begin
      readSwap <= 1'b0;
      mStartTransaction <= 1'b1;
    end

    state[RWOb]: begin
      sDataOut[7:0] <= sDataOut_int[7:0];
    end

    state[RWOc]: begin
      mAddress <= mAddress + 2'b10;
      readSwap <= 1'b0;
      mStartTransaction <= 1'b1;
    end

    state[RWOd]: begin
      sDataOut[15:8] <= sDataOut_int[15:8];
      sAck <= mAck;
    end

    state[WB]: begin 
      writeSwap <= !A0;
      mLowerMask <= !A0;
      mUpperMask <= A0;
      mWriteEnable <= 1'b1;
      mStartTransaction <= 1'b1;
    end

    state[WWE]: begin
      writeSwap <= 1'b1;
      mLowerMask <= 1'b0;
      mUpperMask <= 1'b0;
      mWriteEnable <= 1'b1;
      mStartTransaction <= 1'b1;
    end

    state[WWOa]: begin
      writeSwap <= 1'b0;
      mLowerMask <= 1'b0;
      mUpperMask <= 1'b1;
      mWriteEnable <= 1'b1;
      mStartTransaction <= 1'b1;
    end

    state[WWOb]: begin
      mWriteEnable <= 1'b1;
      mStartTransaction <= 1'b0;
    end

    state[WWOc]: begin
      mAddress <= sAddress + 2'b10;
      writeSwap <= 1'b0;
      mLowerMask <= 1'b1;
      mUpperMask <= 1'b0;
      mWriteEnable <= 1'b1;
      mStartTransaction <= 1'b1;
    end

    state[W_ACK]: begin
      mStartTransaction <= 1'b0;
      sAck <= mAck;
    end
  endcase
end
endmodule
