module BYTE_SWAP(
  input wire Swap,
  input wire [15:0] DataIn,
  
  output wire [15:0] DataOut
);

reg [7:0] LSBOut;
reg [7:0] MSBOut;
wire [7:0] LSBIn;
wire [7:0] MSBIn;

assign DataOut = {MSBOut, LSBOut};
assign LSBIn = DataIn[7:0];
assign MSBIn = DataIn[15:8];


always @(*) begin
  if(Swap == 1'b1) begin
    LSBOut <= MSBIn;
    MSBOut <= LSBIn;
  end else begin
    LSBOut <= LSBIn;
    MSBOut <= MSBIn;
  end
end
endmodule
