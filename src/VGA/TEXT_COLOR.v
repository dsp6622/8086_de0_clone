module TEXT_COLOR(
	input wire clk,											//VGA clock
	input wire blink,										//1 bit, blink if on
	input wire [2:0] bgcolor,								//Background color in
	input wire [3:0] fgcolor,								//Foreground color in
	input wire pixelIn,										//Foreground if 1, background if 0.
	output wire [3:0] rPixelOut, gPixelOut, bPixelOut		 //Color to output
);

reg    [11:0] RGBOut;   //Color to output



//Delay by two pixel clocks (One of these delays should probably be outside this module... Oh well)
reg [2:0] bgcolorDelay2;
reg [3:0] fgcolorDelay2;
reg       blinkDelay2;

reg [2:0]  bgcolorDelay;
reg [3:0]  fgcolorDelay;
reg        blinkDelay;
reg [23:0] blink_counter;

assign rPixelOut = RGBOut[11:8];
assign gPixelOut = RGBOut[7:4 ];
assign bPixelOut = RGBOut[3:0 ];

always @(posedge clk) begin
	if(blink_counter == 24'b111111111111111111111111) begin
		blink_counter <= 24'b0;
	end else begin
		blink_counter <= blink_counter + 1'b1;
	end
end

//A whole big section dedicated to assigning color values./////////////////////////////////////////
//Color configuration, RRRRGGGGBBBB
reg[11:0] fgColor0, fgColor1, fgColor2, fgColor3, fgColor4, fgColor5, fgColor6, fgColor7, fgColor8, 
	 fgColor9, fgColor10, fgColor11, fgColor12, fgColor13, fgColor14, fgColor15;
reg[11:0] bgColor0, bgColor1, bgColor2, bgColor3, bgColor4, bgColor5, bgColor6, bgColor7, bgColor8;

parameter black         = 12'h000;
parameter blue          = 12'h00F;
parameter green         = 12'h0F0;
parameter cyan          = 12'h0EE;
parameter red           = 12'hF00;
parameter magenta       = 12'hE0E;
parameter brown         = 12'h850;
parameter lightGray     = 12'h777;
parameter gray          = 12'h555;
parameter lightBlue     = 12'h55f;
parameter lightGreen    = 12'h5F5;
parameter lightCyan     = 12'h6FF;
parameter lightRed      = 12'hF55;
parameter lightMagenta  = 12'hF6F;
parameter yellow        = 12'hFF0;
parameter white         = 12'hFFF;

always @(posedge clk) begin
  //These can be edited in the future to allow arbitrary color assignments. For now, they will be fixed.
  //They could be bypassed, and the parameters could be used in the color assignment block, but they will
  //be left here to make future changes easier.
  //Blinking will also be implemented here.
  fgColor0  <= black;
  fgColor1  <= blue;
  fgColor2  <= green;
  fgColor3  <= cyan;
  fgColor4  <= red;
  fgColor5  <= magenta;
  fgColor6  <= brown;
  fgColor7  <= lightGray;
  fgColor8  <= gray;
  fgColor9  <= lightBlue;
  fgColor10 <= lightGreen;
  fgColor11 <= lightCyan;
  fgColor12 <= lightRed;
  fgColor13 <= lightMagenta;
  fgColor14 <= yellow;
  fgColor15 <= white;
  
  bgColor0  <= black;
  bgColor1  <= blue;
  bgColor2  <= green;
  bgColor3  <= cyan;
  bgColor4  <= red;
  bgColor5  <= magenta;
  bgColor6  <= brown;
  bgColor7  <= lightGray;
end  
///////////////////////////////////////////////////////////////////////////////////////////////////

//Delay the color by one pixel clock and assign foreground/background colors.
always @(posedge clk) begin
	bgcolorDelay2 <= bgcolor;
	bgcolorDelay <= bgcolorDelay2;
	fgcolorDelay2 <= fgcolor;
	fgcolorDelay <= fgcolorDelay2;
	blinkDelay2 <= blink;
	blinkDelay <= blinkDelay2;
  if(pixelIn == 1'b1 && (blink_counter[23] || !blinkDelay)) begin 
    case(fgcolorDelay) 
      0  : RGBOut <= fgColor15;
      1  : RGBOut <= fgColor1;
      2  : RGBOut <= fgColor2;
      3  : RGBOut <= fgColor3;
      4  : RGBOut <= fgColor4;
      5  : RGBOut <= fgColor5;
      6  : RGBOut <= fgColor6;
      7  : RGBOut <= fgColor7;
      8  : RGBOut <= fgColor8;
      9  : RGBOut <= fgColor9;
      10 : RGBOut <= fgColor10;
      11 : RGBOut <= fgColor11;
      12 : RGBOut <= fgColor12;
      13 : RGBOut <= fgColor13;
      14 : RGBOut <= fgColor14;
      15 : RGBOut <= fgColor0;      
      default : RGBOut <= fgColor15;
    endcase
  end else begin
    case(bgcolorDelay) 
      0: RGBOut <= bgColor0;
      1: RGBOut <= bgColor1;
      2: RGBOut <= bgColor2;
      3: RGBOut <= bgColor3;
      4: RGBOut <= bgColor4;
      5: RGBOut <= bgColor5;
      6: RGBOut <= bgColor6;
      7: RGBOut <= bgColor7;
      default : RGBOut <= bgColor7;
    endcase
  end
end

endmodule
