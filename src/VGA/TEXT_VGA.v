// YAMI V1.0 Compliant

module TEXT_VGA(
input wire nReset,
input wire VGAClk,
input wire SysClk,
input wire [14:0] Address,
input wire [15:0] DataIn,
input wire WriteEnable,
input wire UpperMask,
input wire LowerMask,
input wire Start,
output wire [15:0] DataOut,
output wire DataReady,
output wire HS_OUT,
output wire VS_OUT,
output wire [3:0] VGA_R,
output wire [3:0] VGA_G,
output wire [3:0] VGA_B
);


wire [3:0] rPixelOut, gPixelOut, bPixelOut;
wire DE;


wire [7:0 ] Attrib;       //Blink, background color, foreground color
wire [14:0] FontMemoryIn; //Address bus for VRAM
wire [15:0] VRamOut;      //Data bus for VRAM
wire [11:0] H_ACTIVE;     //Horizontal active pixel count
wire [11:0] V_ACTIVE;     //Vertical active line count
wire TextPixelOut; //Output of Font ROM

//Delay VGA timing three cycles to account for memory latency
reg  HS_OUT_DELAY1, VS_OUT_DELAY1, DE_DELAY1;
reg  HS_OUT_DELAY2, VS_OUT_DELAY2, DE_DELAY2;
reg  HS_OUT_DELAY3, VS_OUT_DELAY3, DE_DELAY3;
reg  HS_OUT_DELAY4, VS_OUT_DELAY4, DE_DELAY4;
reg  Start_Reg;
wire HS_OUT_DELAY, VS_OUT_DELAY, DE_DELAY;
wire [13:0]vram_addressa;
wire [13:0]vram_addressb;
wire [1:0] byteEn;
assign byteEn = {UpperMask, LowerMask};
assign HS_OUT = HS_OUT_DELAY4;
assign VS_OUT = VS_OUT_DELAY4;

//Assign VGA Outputs
assign VGA_R = (DE_DELAY4 == 1'b1) ? {rPixelOut} : 4'b0;
assign VGA_G = (DE_DELAY4 == 1'b1) ? {gPixelOut} : 4'b0;
assign VGA_B = (DE_DELAY4 == 1'b1) ? {bPixelOut} : 4'b0;


wire [2:0] lowerFont; //Used to avoid subtraction overflow effects
assign lowerFont          = H_ACTIVE[2:0] - 2'b10;        //Delay horizontal pixel by two clocks
assign FontMemoryIn[6:0]  = ({V_ACTIVE[3:0], lowerFont}); //Least significant bits select individual pixels of a glyph in font ROM (Horizontal and Vertical counters)
assign FontMemoryIn[14:7] = VRamOut[15:8];                 //Most significant byte selects the glyph in font ROM. 
assign Attrib             = VRamOut[7:0];                //Least significant byte VRAM Data is color and blink attributes.
assign vram_addressb      = (V_ACTIVE[9:4] * 14'd80) + (H_ACTIVE[10:3]);
assign vram_addressa      = Address[14:1]; //This will get something else for CGA mode

//assign addressValid       = (Address <= `CGA_UPPER_ADDRESS) && (Address >= `CGA_LOWER_ADDRESS);

//VRAM
VRam VideoRam(
	.address_a		(vram_addressa), //Multiplier is synthesized out into a combination of adders. This could be rewritten with pulse counters.
	.address_b		(vram_addressb),
  .byteena_a  (byteEn),
	.clock_a		(SysClk),
	.clock_b		(VGAClk),
	.data_a			(DataIn),
	.data_b			(16'b0),
	.wren_a			(WriteEnable),
	.wren_b			(1'b0),
	.q_a			(DataOut),
	.q_b			(VRamOut)
);

//Stores the actual character glyphs.
TextROM FontROM (
	.address		(FontMemoryIn),
	.clock			(VGAClk),
	.q				(TextPixelOut)
);

//Controls color and blink of individual characters. 
TEXT_COLOR colorBlink (
	.clk			(VGAClk),
	.blink			(Attrib[7]), //VRamOut
	.bgcolor		(Attrib[6:4]),
	.fgcolor		(Attrib[3:0]),
	.pixelIn		(TextPixelOut),
	.rPixelOut		(rPixelOut),
	.gPixelOut (gPixelOut),
	.bPixelOut (bPixelOut)
);


//Delay VGA timing four cycles to account for memory and color latency
always @(posedge VGAClk) begin
  HS_OUT_DELAY1 <= HS_OUT_DELAY;
  VS_OUT_DELAY1 <= VS_OUT_DELAY;
  DE_DELAY1		 <= DE_DELAY;
  HS_OUT_DELAY2 <= HS_OUT_DELAY1;
  VS_OUT_DELAY2 <= VS_OUT_DELAY1;
  DE_DELAY2     <= DE_DELAY1;
  HS_OUT_DELAY3 <= HS_OUT_DELAY2;
  VS_OUT_DELAY3 <= VS_OUT_DELAY2;
  DE_DELAY3     <= DE_DELAY2; 
  HS_OUT_DELAY4 <= HS_OUT_DELAY3;
  VS_OUT_DELAY4 <= VS_OUT_DELAY3;
  DE_DELAY4     <= DE_DELAY3;   
end
always @(posedge SysClk) begin
  Start_Reg     <= Start; //For acknowledge edge detector
end
assign DataReady = !Start && Start_Reg;
/////////////////////////////////////////////
//Generate VGA timing
VGA640x400 vidya(
.nReset				(nReset),
.SysClk				(VGAClk),
.HS_OUT				(HS_OUT_DELAY),
.VS_OUT				(VS_OUT_DELAY),
.H_ACTIVE			(H_ACTIVE),
.V_ACTIVE			(V_ACTIVE),
.DE_OUT				(DE_DELAY)
);

endmodule
