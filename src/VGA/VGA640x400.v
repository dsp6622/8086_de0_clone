/*
640x400@70Hz for use with 80x25 Text mode (8x16 character set)
*/

module VGA640x400(
input wire nReset,
input wire SysClk,
output reg HS_OUT,
output reg VS_OUT,
output reg [11:0] H_ACTIVE,
output reg [11:0] V_ACTIVE,
output reg DE_OUT
);



//Horizontal parameters measured in pixels
parameter H_back_porch = 48;
parameter H_active_count = 640;
parameter H_front_porch = 16;
parameter H_sync_count = 96;
parameter H_total_pix = 800;

//Vertical parameters measured in lines
parameter V_back_porch = 35;
parameter V_active_count = 400;
parameter V_front_porch = 12;
parameter V_sync_count = 2;

parameter Total_active_pix = 256000;





reg [11:0] Line_counter;

reg [11:0] V_back;
reg [8:0 ] V_sync;
reg [7:0 ] V_front;

reg H_enable;
reg [11:0] H_back;
reg [8:0 ] H_sync;
reg [7:0 ] H_front;




//Signal generator
always @(posedge SysClk, negedge nReset) begin
	if(!nReset) begin
		H_enable <= 0;
		VS_OUT <= 1;
		HS_OUT <= 1;
		Line_counter <= 0;
		H_back <= 0;
		H_sync <= 0;
		H_ACTIVE <= 0;
		H_front <= 0;
		V_back <= 0;
		V_sync <= 0;
		V_ACTIVE <= 0;
		V_front <= 0;
	end		
   else begin
	
	//Vertical timing
	if (Line_counter != H_total_pix) begin
		Line_counter <= Line_counter + 1'b1;
	end
	if (Line_counter == H_total_pix) begin	
		Line_counter <= 1'b0;
		if (V_sync != V_sync_count) begin
			V_sync <= V_sync + 1'b1;
			VS_OUT <= 1'b0;
		end
		if (V_sync == V_sync_count && V_back != V_back_porch) begin
			V_back <= V_back + 1'b1;
			VS_OUT <= 1'b1;
		end
		if (V_back == V_back_porch && V_ACTIVE != V_active_count) begin
			V_ACTIVE <= V_ACTIVE + 1'b1;
			H_enable <= 1'b1;
		end
		if (V_ACTIVE == V_active_count && V_front != V_front_porch) begin
			V_front <= V_front + 1'b1;
			H_enable <= 1'b0;
		end
		if (V_front == V_front_porch) begin
			V_back <= 0;
			V_sync <= 0;
			V_ACTIVE <= 0;
			V_front <= 0;
		end
		
	end
	
	//Horizontal timing
	if (H_sync != H_sync_count) begin
		H_sync <= H_sync + 1'b1;
		HS_OUT <= 1'b0;		
	end
	if (H_sync == H_sync_count && H_back != H_back_porch) begin
		H_back <= H_back + 1'b1;
		HS_OUT <= 1'b1;
	end
	if (H_back == H_back_porch && H_ACTIVE != H_active_count) begin
		H_ACTIVE <= H_ACTIVE + 1'b1;
		if(H_enable) begin	
		  DE_OUT <= 1'b1;
		end
	end
	if (H_ACTIVE == H_active_count && H_front != H_front_porch) begin
		H_front <= H_front + 1'b1;
		DE_OUT <= 1'b0;
	end
	if (H_front == H_front_porch) begin
		H_back <= 0;
		H_sync <= 0;
		H_ACTIVE <= 0;
		H_front <= 0;
	end
end
end
	
endmodule












