

#compile all of the work files

vlog ../altera_mf.v
vlog ../CPU/ALU.v
vlog ../CPU/CPU.v
vlog ../CPU/effective_address.v
vlog ../CPU/GP_REGISTER_FILE.v
vlog ../MEMORY/SDRAM_CTRL.v
vlog ../MEMORY/Bios.v
vlog ../MEMORY/Bios_bb.v
vlog ../MEMORY/Bios_syn.v
vlog ../MEMORY/BiosWrap.v
vlog ../CROSSBAR/CROSSBAR.v
vlog ../LITTLE_ENDIAN/LITTLE_ENDIAN_TOP.v
vlog ../LITTLE_ENDIAN/BYTE_SWAP.v
vlog ../PS2/PS2_Controller.v
vlog ../SPI/SPI.v
vlog ../VGA/TEXT_COLOR.v
vlog ../VGA/TextROM.v
vlog ../VGA/TextROM_bb.v
vlog ../VGA/TEXT_VGA.v
vlog ../VGA/VGA640x400.v
vlog ../VGA/VGAPLL.v
vlog ../VGA/VGAPLL_bb.v
vlog ../VGA/VRam.v
vlog ../VGA/VRam_bb.v
vlog ../VGA/VRam_syn.v
vlog ../Top.v

#start simulation

vsim -L altera_mf_ver -L lpm_ver -L cycloneiii_ver work.Top
add wave -radix hex \
sim:/Top/SysClk \
sim:/Top/RESET2 \
sim:/Top/DRAM_ADDR \
sim:/Top/DRAM_DQ \
sim:/Top/DRAM_BA \
sim:/Top/DRAM_LDQM \
sim:/Top/DRAM_UDQM \
sim:/Top/DRAM_WE_N \
sim:/Top/DRAM_CAS_N \
sim:/Top/DRAM_RAS_N \
sim:/Top/DRAM_CS_N \
sim:/Top/DRAM_CKE \
sim:/Top/PS2_KBCLK \
sim:/Top/PS2_KBDAT \
sim:/Top/VGA_CLOCK \
sim:/Top/CPU_DataReady \
sim:/Top/CPU_DataIn \
sim:/Top/CPU_DataOut \
sim:/Top/CPU_AddressOut \
sim:/Top/WordByte \
sim:/Top/CPU_WE \
sim:/Top/CPU_Start \
sim:/Top/_8086/state \
sim:/Top/_8086/gp_registers/AX \
sim:/Top/_8086/gp_registers/CX \
sim:/Top/_8086/gp_registers/DX \
sim:/Top/_8086/gp_registers/BX \
sim:/Top/_8086/gp_registers/SP \
sim:/Top/_8086/gp_registers/BP \
sim:/Top/_8086/gp_registers/SI \
sim:/Top/_8086/gp_registers/DI \
sim:/Top/endianConverter/state \
sim:/Top/BusAddress \
sim:/Top/BusDataIn \
sim:/Top/BusWE \
sim:/Top/BusUpperMask \
sim:/Top/BusLowerMask \
sim:/Top/BusStartTransaction \
sim:/Top/BusDataOut \
sim:/Top/BusAck \
sim:/Top/VideoAddress \
sim:/Top/VideoWriteData \
sim:/Top/VideoWE \
sim:/Top/VideoStart \
sim:/Top/VideoUM \
sim:/Top/VideoLM \
sim:/Top/VideoReadData \
sim:/Top/VideoAck \
sim:/Top/RamAddress \
sim:/Top/RamWriteData \
sim:/Top/RamWE \
sim:/Top/RamStart \
sim:/Top/RamUM \
sim:/Top/RamLM \
sim:/Top/RamReadData \
sim:/Top/RamAck \
sim:/Top/BiosAddress \
sim:/Top/BiosWriteData \
sim:/Top/BiosWE \
sim:/Top/BiosStart \
sim:/Top/BiosUM \
sim:/Top/BiosLM \
sim:/Top/BiosReadData \
sim:/Top/BiosAck

do TOP_STIM.do
