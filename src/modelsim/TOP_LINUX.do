# This script only needs to be run once, then TOP.do should work

#create the work library
vlib work


#map in external libraries (you may have to call in altera libraries here)
vmap cycloneiii /vsim-ase/altera/verilog/cycloneiii

#Refreshing by tcl doesn't appear to work... Just refresh it from the gui
vcom -work /vsim-ase/altera/verilog/cycloneiii -refresh -force_refresh
vcom -reportprogress 300 -work /vsim-ase/altera/verilog/cycloneiii -refresh -force_refresh

do TOP.do
