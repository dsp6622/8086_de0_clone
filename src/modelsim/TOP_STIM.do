restart

force -freeze sim:/Top/RESET2 0 0
force -freeze sim:/Top/SysClk 1 0, 0 {5000 ps} -r {10 ns}
force -freeze sim:/Top/VGA_CLOCK 1 0, 0 {5000 ps} -r {40 ns}
run 40 ns
force -freeze sim:/Top/RESET2 1 0

run 6500 ns
