# Copyright (C) 1991-2013 Altera Corporation
# Your use of Altera Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License 
# Subscription Agreement, Altera MegaCore Function License 
# Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the 
# applicable agreement for further details.

# Quartus II: Generate Tcl File for Project
# File: project.tcl
# Generated on: Sat Feb  3 09:28:10 2018

# Load Quartus II Tcl Project package
package require ::quartus::project

set need_to_close_project 0
set make_assignments 1

# Check that the right project is open
if {[is_project_open]} {
	if {[string compare $quartus(project) "8086_clone"]} {
		puts "Project 8086_clone is not open"
		set make_assignments 0
	}
} else {
	# Only open if not already open
	if {[project_exists 8086_clone]} {
		project_open -revision Top 8086_clone
	} else {
		project_new -revision Top 8086_clone
	}
	set need_to_close_project 1
}

# Make assignments
if {$make_assignments} {
	set_global_assignment -name FAMILY "Cyclone III"
	set_global_assignment -name DEVICE EP3C16F484C6
	set_global_assignment -name ORIGINAL_QUARTUS_VERSION 13.1
	set_global_assignment -name PROJECT_CREATION_TIME_DATE "17:41:24  JUNE 18, 2016"
	set_global_assignment -name LAST_QUARTUS_VERSION 13.1
	set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files
	set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
	set_global_assignment -name MAX_CORE_JUNCTION_TEMP 85
	set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 1
	set_global_assignment -name EDA_SIMULATION_TOOL "ModelSim-Altera (Verilog)"
	set_global_assignment -name EDA_OUTPUT_DATA_FORMAT "VERILOG HDL" -section_id eda_simulation
	set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
	set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT_AND_ROUTING -section_id Top
	set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
	set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
	set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
	set_global_assignment -name STRATIX_DEVICE_IO_STANDARD "2.5 V"
	set_global_assignment -name ENABLE_CONFIGURATION_PINS OFF
	set_global_assignment -name ENABLE_NCE_PIN OFF
	set_global_assignment -name ENABLE_BOOT_SEL_PIN OFF
	set_global_assignment -name USE_CONFIGURATION_DEVICE OFF
	set_global_assignment -name CRC_ERROR_OPEN_DRAIN OFF
	set_global_assignment -name CYCLONEII_RESERVE_NCEO_AFTER_CONFIGURATION "USE AS REGULAR IO"
	set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -rise
	set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -fall
	set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -rise
	set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -fall
	set_global_assignment -name EDA_TIME_SCALE "1 ps" -section_id eda_simulation
	set_global_assignment -name ENABLE_SIGNALTAP OFF
	set_global_assignment -name USE_SIGNALTAP_FILE output_files/stp2.stp
	set_global_assignment -name VERILOG_FILE ../CROSSBAR/CROSSBAR.v
	set_global_assignment -name VERILOG_FILE ../LITTLE_ENDIAN/BYTE_SWAP.v
	set_global_assignment -name VERILOG_FILE ../LITTLE_ENDIAN/LITTLE_ENDIAN_TOP.v
	set_global_assignment -name VERILOG_FILE ../MEMORY/BiosWrap.v
	set_global_assignment -name VERILOG_FILE ../CPU/ALU.v
	set_global_assignment -name VERILOG_FILE ../MEMORY/MEMORY_MAP.v
	set_global_assignment -name VERILOG_FILE ../CPU/GP_REGISTER_FILE.v
	set_global_assignment -name VERILOG_FILE ../CPU/CPU.v
	set_global_assignment -name HEX_FILE ../VGA/VRam.hex
	set_global_assignment -name MIF_FILE ../VGA/VGAFont.mif
	set_global_assignment -name QIP_FILE ../VGA/VGAPLL.qip
	set_global_assignment -name QIP_FILE ../VGA/VRam.qip
	set_global_assignment -name VERILOG_FILE ../VGA/VGA640x400.v
	set_global_assignment -name VERILOG_FILE ../Top.v
	set_global_assignment -name VERILOG_FILE ../VGA/TEXT_VGA.v
	set_global_assignment -name VERILOG_FILE ../VGA/TEXT_COLOR.v
	set_global_assignment -name VERILOG_FILE ../SPI/SPI.v
	set_global_assignment -name VERILOG_FILE ../MEMORY/SDRAM_CTRL.v
	set_global_assignment -name VERILOG_FILE ../PS2/PS2_Controller.v
	set_global_assignment -name QIP_FILE ../VGA/TextROM.qip
	set_global_assignment -name SDC_FILE ../8086_clone.sdc
	set_global_assignment -name QIP_FILE ../MEMORY/Bios.qip
	set_global_assignment -name MIF_FILE ../MEMORY/BIOS.mif
	set_location_assignment PIN_B1 -to LEDG[9]
	set_location_assignment PIN_B2 -to LEDG[8]
	set_location_assignment PIN_C2 -to LEDG[7]
	set_location_assignment PIN_C1 -to LEDG[6]
	set_location_assignment PIN_E1 -to LEDG[5]
	set_location_assignment PIN_F2 -to LEDG[4]
	set_location_assignment PIN_H1 -to LEDG[3]
	set_location_assignment PIN_J3 -to LEDG[2]
	set_location_assignment PIN_J2 -to LEDG[1]
	set_location_assignment PIN_J1 -to LEDG[0]
	set_location_assignment PIN_G21 -to CLOCK_50
	set_location_assignment PIN_K16 -to VGA_B[3]
	set_location_assignment PIN_J22 -to VGA_B[2]
	set_location_assignment PIN_K21 -to VGA_B[1]
	set_location_assignment PIN_K22 -to VGA_B[0]
	set_location_assignment PIN_J21 -to VGA_G[3]
	set_location_assignment PIN_K17 -to VGA_G[2]
	set_location_assignment PIN_J17 -to VGA_G[1]
	set_location_assignment PIN_H22 -to VGA_G[0]
	set_location_assignment PIN_H21 -to VGA_R[3]
	set_location_assignment PIN_H20 -to VGA_R[2]
	set_location_assignment PIN_H17 -to VGA_R[1]
	set_location_assignment PIN_H19 -to VGA_R[0]
	set_location_assignment PIN_L21 -to VGA_HS
	set_location_assignment PIN_L22 -to VGA_VS
	set_location_assignment PIN_H2 -to BUTTON0
	set_location_assignment PIN_J6 -to SWITCHES[0]
	set_location_assignment PIN_D2 -to SWITCHES[9]
	set_location_assignment PIN_E4 -to SWITCHES[8]
	set_location_assignment PIN_E3 -to SWITCHES[7]
	set_location_assignment PIN_H7 -to SWITCHES[6]
	set_location_assignment PIN_J7 -to SWITCHES[5]
	set_location_assignment PIN_G5 -to SWITCHES[4]
	set_location_assignment PIN_G4 -to SWITCHES[3]
	set_location_assignment PIN_H6 -to SWITCHES[2]
	set_location_assignment PIN_H5 -to SWITCHES[1]
	set_location_assignment PIN_G3 -to BUTTON1
	set_location_assignment PIN_F1 -to BUTTON2
	set_location_assignment PIN_P22 -to PS2_KBCLK
	set_location_assignment PIN_P21 -to PS2_KBDAT
	set_location_assignment PIN_AA20 -to GPIO1_D0
	set_location_assignment PIN_AB20 -to GPIO1_D1
	set_location_assignment PIN_AA19 -to GPIO1_D2
	set_location_assignment PIN_AB19 -to GPIO1_D3
	set_location_assignment PIN_G8 -to DRAM_CAS_N
	set_location_assignment PIN_G7 -to DRAM_CS_N
	set_location_assignment PIN_E5 -to DRAM_CLK
	set_location_assignment PIN_E6 -to DRAM_CKE
	set_location_assignment PIN_B5 -to DRAM_BA_0
	set_location_assignment PIN_A4 -to DRAM_BA_1
	set_location_assignment PIN_F10 -to DRAM_DQ[15]
	set_location_assignment PIN_E10 -to DRAM_DQ[14]
	set_location_assignment PIN_A10 -to DRAM_DQ[13]
	set_location_assignment PIN_B10 -to DRAM_DQ[12]
	set_location_assignment PIN_C10 -to DRAM_DQ[11]
	set_location_assignment PIN_A9 -to DRAM_DQ[10]
	set_location_assignment PIN_B9 -to DRAM_DQ[9]
	set_location_assignment PIN_A8 -to DRAM_DQ[8]
	set_location_assignment PIN_F8 -to DRAM_DQ[7]
	set_location_assignment PIN_H9 -to DRAM_DQ[6]
	set_location_assignment PIN_G9 -to DRAM_DQ[5]
	set_location_assignment PIN_F9 -to DRAM_DQ[4]
	set_location_assignment PIN_E9 -to DRAM_DQ[3]
	set_location_assignment PIN_H10 -to DRAM_DQ[2]
	set_location_assignment PIN_G10 -to DRAM_DQ[1]
	set_location_assignment PIN_D10 -to DRAM_DQ[0]
	set_location_assignment PIN_E7 -to DRAM_LDQM
	set_location_assignment PIN_B8 -to DRAM_UDQM
	set_location_assignment PIN_F7 -to DRAM_RAS_N
	set_location_assignment PIN_D6 -to DRAM_WE_N
	set_location_assignment PIN_B12 -to CLOCK_50_2
	set_location_assignment PIN_C8 -to DRAM_ADDR[12]
	set_location_assignment PIN_A7 -to DRAM_ADDR[11]
	set_location_assignment PIN_B4 -to DRAM_ADDR[10]
	set_location_assignment PIN_B7 -to DRAM_ADDR[9]
	set_location_assignment PIN_C7 -to DRAM_ADDR[8]
	set_location_assignment PIN_A6 -to DRAM_ADDR[7]
	set_location_assignment PIN_B6 -to DRAM_ADDR[6]
	set_location_assignment PIN_C6 -to DRAM_ADDR[5]
	set_location_assignment PIN_A5 -to DRAM_ADDR[4]
	set_location_assignment PIN_C3 -to DRAM_ADDR[3]
	set_location_assignment PIN_B3 -to DRAM_ADDR[2]
	set_location_assignment PIN_A3 -to DRAM_ADDR[1]
	set_location_assignment PIN_C4 -to DRAM_ADDR[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to CLOCK_50
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to CLOCK_50_2
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to BUTTON[2]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to BUTTON[1]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to BUTTON[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[2]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[3]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[4]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[5]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[6]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[7]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[8]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[9]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[10]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[11]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[12]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[13]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[14]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[15]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_CS_N
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_CLK
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_CKE
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_CAS_N
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_BA_1
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_BA_0
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[1]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[2]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[3]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[4]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[5]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[6]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[7]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[8]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[9]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[10]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[11]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_ADDR[12]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_WE_N
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_UDQM
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_RAS_N
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_LDQM
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to DRAM_DQ[1]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to PS2_MSDAT
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to PS2_MSCLK
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to PS2_KBDAT
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to PS2_KBCLK
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_VS
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[1]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[2]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[3]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_HS
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[1]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[2]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[3]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[1]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[2]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[3]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[0]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[1]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[2]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[3]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[4]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[5]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[6]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[7]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[8]
	set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to SW[9]
	set_instance_assignment -name CURRENT_STRENGTH_NEW 12MA -to GPIO1_D0
	set_instance_assignment -name CURRENT_STRENGTH_NEW 12MA -to GPIO1_D1
	set_instance_assignment -name CURRENT_STRENGTH_NEW 12MA -to GPIO1_D2
	set_instance_assignment -name CURRENT_STRENGTH_NEW 12MA -to GPIO1_D3
	set_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top

	# Commit assignments
	export_assignments

	# Close project
	if {$need_to_close_project} {
		project_close
	}
}
