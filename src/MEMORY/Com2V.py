import binascii
filename = 'program.com'
outfilename = 'program.v'

o=open(outfilename, 'w')
    
with open(filename, 'rb') as f:
    content = f.read()
com = binascii.b2a_hex(content)
j = 0
o.write('case(CPU_AddressOut)\n')
for i in range(0, len(com)-2,2):
    address = j+1048560
    j = j + 1
    if address >= 1048576:
        address = address - 1048576
    payload = com[i:i+7]
    o.write('20\'h' + '%05x'%address + ':\n  CPU_DataIn <= 16\'h' + str(payload)[2:6] + ';\n')

#Ugly way of ending in a NOP
i = i + 1
payload = com[i:i+7]
address = j+1048560
j = j + 1
if address >= 1048576:
    address = address - 1048576
o.write('20\'h' + '%05x'%address + ':\n  CPU_DataIn <= 16\'h' + str(payload)[2:4] + '09' + ';\n')
        

o.write('default:\n  CPU_DataIn <= Vram_DataOut;\n')

o.write('endcase')
o.close()
