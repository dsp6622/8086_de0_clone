case(CPU_AddressOut)
20'hffff0:
  CPU_DataIn <= 16'hbb00;
20'hffff1:
  CPU_DataIn <= 16'h00b8;
20'hffff2:
  CPU_DataIn <= 16'hb88e;
20'hffff3:
  CPU_DataIn <= 16'h8edb;
20'hffff4:
  CPU_DataIn <= 16'hdb8e;
20'hffff5:
  CPU_DataIn <= 16'h8ed3;
20'hffff6:
  CPU_DataIn <= 16'hd3bc;
20'hffff7:
  CPU_DataIn <= 16'hbc32;
20'hffff8:
  CPU_DataIn <= 16'h3200;
20'hffff9:
  CPU_DataIn <= 16'h00c7;
20'hffffa:
  CPU_DataIn <= 16'hc706;
20'hffffb:
  CPU_DataIn <= 16'h0606;
20'hffffc:
  CPU_DataIn <= 16'h0600;
20'hffffd:
  CPU_DataIn <= 16'h0041;
20'hffffe:
  CPU_DataIn <= 16'h4114;
20'hfffff:
  CPU_DataIn <= 16'h14c7;
20'h00000:
  CPU_DataIn <= 16'hc706;
20'h00001:
  CPU_DataIn <= 16'h0606;
20'h00002:
  CPU_DataIn <= 16'h0608;
20'h00003:
  CPU_DataIn <= 16'h0858;
20'h00004:
  CPU_DataIn <= 16'h58f1;
20'h00005:
  CPU_DataIn <= 16'hf1a0;
20'h00006:
  CPU_DataIn <= 16'ha006;
20'h00007:
  CPU_DataIn <= 16'h0600;
20'h00008:
  CPU_DataIn <= 16'h00a1;
20'h00009:
  CPU_DataIn <= 16'ha106;
20'h0000a:
  CPU_DataIn <= 16'h0600;
20'h0000b:
  CPU_DataIn <= 16'h00c7;
20'h0000c:
  CPU_DataIn <= 16'hc706;
20'h0000d:
  CPU_DataIn <= 16'h0604;
20'h0000e:
  CPU_DataIn <= 16'h0400;
20'h0000f:
  CPU_DataIn <= 16'h0000;
20'h00010:
  CPU_DataIn <= 16'h0000;
20'h00011:
  CPU_DataIn <= 16'h00a2;
20'h00012:
  CPU_DataIn <= 16'ha204;
20'h00013:
  CPU_DataIn <= 16'h0400;
20'h00014:
  CPU_DataIn <= 16'h00a3;
20'h00015:
  CPU_DataIn <= 16'ha302;
20'h00016:
  CPU_DataIn <= 16'h0200;
20'h00017:
  CPU_DataIn <= 16'h00bb;
20'h00018:
  CPU_DataIn <= 16'hbb59;
20'h00019:
  CPU_DataIn <= 16'h59f1;
20'h0001a:
  CPU_DataIn <= 16'hf188;
20'h0001b:
  CPU_DataIn <= 16'h88d9;
20'h0001c:
  CPU_DataIn <= 16'hd988;
20'h0001d:
  CPU_DataIn <= 16'h880e;
20'h0001e:
  CPU_DataIn <= 16'h0e08;
20'h0001f:
  CPU_DataIn <= 16'h0800;
20'h00020:
  CPU_DataIn <= 16'h0089;
20'h00021:
  CPU_DataIn <= 16'h89da;
20'h00022:
  CPU_DataIn <= 16'hda8b;
20'h00023:
  CPU_DataIn <= 16'h8bc3;
20'h00024:
  CPU_DataIn <= 16'hc389;
20'h00025:
  CPU_DataIn <= 16'h8916;
20'h00026:
  CPU_DataIn <= 16'h1610;
20'h00027:
  CPU_DataIn <= 16'h1000;
20'h00028:
  CPU_DataIn <= 16'h008b;
20'h00029:
  CPU_DataIn <= 16'h8b1e;
20'h0002a:
  CPU_DataIn <= 16'h1e02;
20'h0002b:
  CPU_DataIn <= 16'h0200;
20'h0002c:
  CPU_DataIn <= 16'h008a;
20'h0002d:
  CPU_DataIn <= 16'h8a16;
20'h0002e:
  CPU_DataIn <= 16'h1606;
20'h0002f:
  CPU_DataIn <= 16'h0600;
20'h00030:
  CPU_DataIn <= 16'h0089;
20'h00031:
  CPU_DataIn <= 16'h891e;
20'h00032:
  CPU_DataIn <= 16'h1e12;
20'h00033:
  CPU_DataIn <= 16'h1200;
20'h00034:
  CPU_DataIn <= 16'h00a3;
20'h00035:
  CPU_DataIn <= 16'ha326;
20'h00036:
  CPU_DataIn <= 16'h2600;
20'h00037:
  CPU_DataIn <= 16'h0088;
20'h00038:
  CPU_DataIn <= 16'h8816;
20'h00039:
  CPU_DataIn <= 16'h1614;
20'h0003a:
  CPU_DataIn <= 16'h1400;
20'h0003b:
  CPU_DataIn <= 16'h008c;
20'h0003c:
  CPU_DataIn <= 16'h8cd9;
20'h0003d:
  CPU_DataIn <= 16'hd98c;
20'h0003e:
  CPU_DataIn <= 16'h8c1e;
20'h0003f:
  CPU_DataIn <= 16'h1e20;
20'h00040:
  CPU_DataIn <= 16'h2000;
20'h00041:
  CPU_DataIn <= 16'h0089;
20'h00042:
  CPU_DataIn <= 16'h890e;
20'h00043:
  CPU_DataIn <= 16'h0e22;
20'h00044:
  CPU_DataIn <= 16'h2200;
20'h00045:
  CPU_DataIn <= 16'h002b;
20'h00046:
  CPU_DataIn <= 16'h2b1e;
20'h00047:
  CPU_DataIn <= 16'h1e06;
20'h00048:
  CPU_DataIn <= 16'h0608;
20'h00049:
  CPU_DataIn <= 16'h0829;
20'h0004a:
  CPU_DataIn <= 16'h29c3;
20'h0004b:
  CPU_DataIn <= 16'hc32b;
20'h0004c:
  CPU_DataIn <= 16'h2bc3;
20'h0004d:
  CPU_DataIn <= 16'hc329;
20'h0004e:
  CPU_DataIn <= 16'h291e;
20'h0004f:
  CPU_DataIn <= 16'h1e02;
20'h00050:
  CPU_DataIn <= 16'h0200;
20'h00051:
  CPU_DataIn <= 16'h0005;
20'h00052:
  CPU_DataIn <= 16'h0515;
20'h00053:
  CPU_DataIn <= 16'h1500;
20'h00054:
  CPU_DataIn <= 16'h002c;
20'h00055:
  CPU_DataIn <= 16'h2c20;
20'h00056:
  CPU_DataIn <= 16'h2029;
20'h00057:
  CPU_DataIn <= 16'h2906;
20'h00058:
  CPU_DataIn <= 16'h0604;
20'h00059:
  CPU_DataIn <= 16'h0400;
20'h0005a:
  CPU_DataIn <= 16'h0050;
20'h0005b:
  CPU_DataIn <= 16'h505b;
20'h0005c:
  CPU_DataIn <= 16'h5b53;
20'h0005d:
  CPU_DataIn <= 16'h5353;
20'h0005e:
  CPU_DataIn <= 16'h53bb;
20'h0005f:
  CPU_DataIn <= 16'hbb59;
20'h00060:
  CPU_DataIn <= 16'h59f1;
20'h00061:
  CPU_DataIn <= 16'hf153;
20'h00062:
  CPU_DataIn <= 16'h5359;
20'h00063:
  CPU_DataIn <= 16'h5990;
20'h00064:
  CPU_DataIn <= 16'h9909;
default:
  CPU_DataIn <= Vram_DataOut;
endcase
