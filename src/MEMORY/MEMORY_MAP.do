

#compile all of the work files

vlog ../altera_mf.v
vlog ../CPU/ALU.v
vlog ../CPU/CPU.v
vlog ../CPU/effective_address.v
vlog ../CPU/GP_REGISTER_FILE.v
vlog ../MEMORY/SDRAM_CTRL.v
vlog ../PS2/PS2_Controller.v
vlog ../SPI/SPI.v
vlog ../VGA/TEXT_COLOR.v
vlog ../VGA/TextROM.v
vlog ../VGA/TextROM_bb.v
vlog ../VGA/TEXT_VGA.v
vlog ../VGA/VGA640x400.v
vlog ../VGA/VGAPLL.v
vlog ../VGA/VGAPLL_bb.v
vlog ../VGA/VRam.v
vlog ../VGA/VRam_bb.v
vlog ../VGA/VRam_syn.v
vlog ../MEMORY/MEMORY_MAP.v

#start simulation

vsim -L altera_mf_ver -L lpm_ver -L cycloneiii_ver work.MEMORY_MAP

add wave -position end -radix hex sim:/MEMORY_MAP/nReset
add wave -position end -radix hex sim:/MEMORY_MAP/DataOut
add wave -position end -radix hex sim:/MEMORY_MAP/HS_OUT
add wave -position end -radix hex sim:/MEMORY_MAP/VS_OUT
add wave -position end -radix hex sim:/MEMORY_MAP/VGA_R
add wave -position end -radix hex sim:/MEMORY_MAP/VGA_G
add wave -position end -radix hex sim:/MEMORY_MAP/VGA_B
add wave -position end -radix hex sim:/MEMORY_MAP/VideoAddress
add wave -position end -radix hex sim:/MEMORY_MAP/VideoData
add wave -position end -radix hex sim:/MEMORY_MAP/CPU_DataIn
add wave -position end -radix hex sim:/MEMORY_MAP/CPU_DataOut
add wave -position end -radix hex sim:/MEMORY_MAP/CPU_AddressOut
add wave -position end -radix hex sim:/MEMORY_MAP/DataReady
add wave -position end -radix hex sim:/MEMORY_MAP/VideoWE
add wave -position end -radix hex sim:/MEMORY_MAP/CPU_WE
add wave -position end -radix hex sim:/MEMORY_MAP/CPU_Start
add wave -position end -radix decimal sim:/MEMORY_MAP/_8086/state
add wave -position end -radix decimal sim:/MEMORY_MAP/_8086/next

add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/DataIn
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/DataOut
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/PushPop
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/WriteEnable
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/AX
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/BX
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/CX
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/DX
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/SP
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/BP
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/SI
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/gp_registers/DI

add wave -position 16 -radix hex sim:/MEMORY_MAP/video/VideoRam/address_b
add wave -position 17 -radix hex sim:/MEMORY_MAP/video/VideoRam/data_b
add wave -position 18 -radix hex sim:/MEMORY_MAP/video/VideoRam/wren_b
add wave -position 19 -radix hex sim:/MEMORY_MAP/video/VideoRam/q_b
add wave -position 20 -radix hex sim:/MEMORY_MAP/video/VideoRam/address_a
add wave -position 21 -radix hex sim:/MEMORY_MAP/video/VideoRam/data_a
add wave -position 22 -radix hex sim:/MEMORY_MAP/video/VideoRam/wren_a
add wave -position 23 -radix hex sim:/MEMORY_MAP/video/VideoRam/q_a
add wave -position 24 -radix hex sim:/MEMORY_MAP/_8086/instruction
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/alu_en
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/alu_in1
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/alu_in2
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/alu_op
add wave -position end -radix hex sim:/MEMORY_MAP/_8086/alu_dout
do MEMORY_MAP_STIM.do
