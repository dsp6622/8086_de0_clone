//synopsys translate on
module MEMORY_MAP(
  
  input wire SysClk,
  input wire VGAClk,
  input wire nReset,
  
  output wire HS_OUT,
  output wire VS_OUT,
  output wire [3:0] VGA_R,
  output wire [3:0] VGA_G,
  output wire [3:0] VGA_B
);
wire [19:0] VideoAddress /* synthesis keep */ ;
wire [15:0] VideoData /* synthesis keep */ ; 
wire [15:0] Vram_DataOut; 
reg [15:0] CPU_DataIn;
wire [15:0] CPU_DataOut;
wire [19:0] CPU_AddressOut;
reg DataReady; //Ack signal for both read and write
reg VideoWE /* synthesis preserve */ ; 
wire CPU_WE, CPU_Start;
assign VideoAddress = CPU_AddressOut;
assign VideoData = CPU_DataOut;

TEXT_VGA video (
.nReset (nReset),
.VGAClk (VGAClk),
.SysClk (SysClk),
.Address (VideoAddress),
.DataIn  (VideoData),
.WriteEnable (VideoWE),
.DataReady (),
.DataOut (Vram_DataOut),
.HS_OUT (HS_OUT),
.VS_OUT (VS_OUT),
.VGA_R (VGA_R),
.VGA_G (VGA_G),
.VGA_B (VGA_B)
);

CPU _8086 (
.SysClk (SysClk),
.nReset (nReset),
.Interrupt (1'b0),
.DataReady (DataReady),
.InterruptIndex (8'b0),
.DataIn (CPU_DataIn),
.DataOut (CPU_DataOut),
.AddressOut (CPU_AddressOut),
.WordByte (), //Word = 1, byte = 0
.WriteEnable (CPU_WE),
.StartTransaction (CPU_Start)
);

parameter IDLE = 0,
          WRITE_VRAMa = 1,
			    WRITE_VRAMb = 2,
          ACK = 3,
          READ_INSTRUCTION = 4;


reg [4:0] state, next /* synthesis preserve */ ;
always @(posedge SysClk, negedge nReset) begin
  if (!nReset) begin
    state <= 4'b0;
    state[IDLE] <= 1'b1;
  end
  else begin 
    state <= next;
  end
end

always @(*) begin
  next = 4'b0;
  case (1'b1) // synopsys parallel_case
    state[IDLE] : begin
      if(CPU_Start == 1'b1) begin
        case(CPU_WE)
        1'b1:
          next[WRITE_VRAMa] = 1'b1;
        default:
          next[READ_INSTRUCTION] = 1'b1;
        endcase
      end else begin
        next[IDLE] = 1'b1;
      end
    end
    
    
    state[WRITE_VRAMa]: begin
      next[WRITE_VRAMb] = 1'b1;
    end
    state[WRITE_VRAMb]: begin
      next[ACK] = 1'b1;
    end
    
    state[READ_INSTRUCTION]: begin
      next[ACK] = 1'b1;
    end
    
    state[ACK]: begin
      next[IDLE] = 1'b1;
    end
    
    default : begin
      next[IDLE] = 1'b1;    
    end
 endcase
 end
 
 
 always@(posedge SysClk) begin
   if(!nReset) begin
     CPU_DataIn <= 16'b0;
     DataReady <= 1'b0;
     VideoWE <= 1'b0;
   end else begin
     CPU_DataIn <= CPU_DataIn;
     DataReady <= DataReady;
     VideoWE <= VideoWE;
    case (1'b1) // synopsys parallel_case
      state[IDLE]: begin 
        DataReady <= 1'b0;
        VideoWE <= 1'b0;
      end
      
      state[READ_INSTRUCTION]: begin
        VideoWE <= VideoWE;
      end
      
      state[WRITE_VRAMa]: begin
        VideoWE <= 1'b1;
      end
      state[WRITE_VRAMb]: begin
        VideoWE <= 1'b1;
      end
      
      state[ACK]: begin

        `include "../MEMORY/program.v"
      
        VideoWE <= 1'b0;
        DataReady <= 1'b1;
      end
      default : begin
		  VideoWE <= 1'b0;
		end
    endcase
    end
end
        
        


endmodule
