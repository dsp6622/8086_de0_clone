#fasm#          ; this code is for flat assembler

name "mycode"   ; output file name (max 8 chars for DOS compatibility)

org  100h	; set location counter to 100h



mov bx, 0b800h        
mov ds, bx     
mov ss, bx
mov sp, 32h        
mov word[6h], 1441h    
mov word[806h], 0F158h        
MOV word[4h], 0h  
MOV AL, byte[6h]	;1010 0000
MOV AX, word[6h]	;1010 0001
MOV byte[4h], AL 	;1010 0000 
MOV word[2h], AX	;1010 0001 
MOV BX, 0F159h      
MOV CL, BL          ;1000 1000
MOV byte[8h], CL    ;1000 1000
MOV DX, BX          ;1000 1001
MOV word[10h], DX   ;1000 1001
MOV BX, word[2h] 
MOV DL, byte[6h]                        
MOV word[12h], BX
MOV byte[14h], DL        
MOV CX, DS
MOV word[20h], DS
MOV word[22h], CX         

PUSH AX
POP BX                  
push BX    
push bx  
mov BX, 0F159h
push bx            
pop cx
                    

nop


