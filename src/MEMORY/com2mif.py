import binascii
filename = 'program.com'
outfilename = 'BIOS.mif'

o=open(outfilename, 'w')
    
with open(filename, 'rb') as f:
    content = f.read()
com = binascii.b2a_hex(content)
j = 0
o.write('WIDTH=16;\nDEPTH=8192;\nADDRESS_RADIX=UNS;\nDATA_RADIX=HEX;\nCONTENT BEGIN\n')
for i in range(0, len(com)-2,4):
    address = j
    j = j + 1
    payload = com[i:i+15]
    o.write(str(address) + ': ' + str(payload)[2:6] + ';\n')

o.write('[' + str(j) + '..8191] : 0;\n')
o.write('END;')

o.close()
