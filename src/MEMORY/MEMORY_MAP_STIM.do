restart

force -freeze sim:/MEMORY_MAP/nReset 0 0
force -freeze sim:/MEMORY_MAP/SysClk 1 0, 0 {5000 ps} -r {10 ns}
force -freeze sim:/MEMORY_MAP/VGAClk 1 0, 0 {5000 ps} -r {40 ns}
run 40 ns
force -freeze sim:/MEMORY_MAP/nReset St1 0

run 6500 ns
