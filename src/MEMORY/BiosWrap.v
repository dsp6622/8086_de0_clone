module BIOS(
  input  wire SysClk,
  input  wire [12:0] Address,
  input  wire [15:0] WriteData,
  input  wire WriteEn,
  input  wire Start,
  input  wire UpperMask,
  input  wire LowerMask,
  output wire [15:0] ReadData,
  output reg  Ack
);

wire [1:0] ByteEnable;
assign ByteEnable = {UpperMask, LowerMask};
reg start_d;

always@(posedge SysClk) begin
  start_d <= Start;
  Ack <= start_d & !Start;
end

Bios BiosRom(
.address (Address),
.byteena (ByteEnable),
.clock (SysClk),
.data (WriteData),
.wren (WriteEn),
.q (ReadData)
);


endmodule

