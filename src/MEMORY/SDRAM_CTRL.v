//YAMI V1.0 Compliant
//Multi-Cycle Non-Interrupting IO




//There are probably two clocks so that the CPU can clock slower than the SDRAM 


module SDRAM_CTRL(
//Controller logic
input wire SysClk,				    //System Clock
input wire [15:0] DataIn,			
input wire [19:0] Address,
input wire WriteEnable,				    //Write enable
input wire Start,			   //Start transaction
input wire niLDQM,
input wire niUDQM,

output reg  Ack,				    //Set to complete transaction
output reg  [15:0] DataOut,			
//SDRAM signals
input wire SDCLK,						//Clock
input wire nReset,					//Active low reset
output wire CLK_EN,					//Clock enable
output reg  [11:0] ADDRE,						//12 bit address port
output wire nChip_sel,		//Chip select, always high for one chip
output reg  n_RAS,
output reg  n_CAS,
output reg  nLDQM,						//Lower data mask
output reg  nUDQM,						//Upper data mask
output reg  [1:0] BA,						//Bank address 
output reg  nWriteEN,					
inout wire [15:0] DQ						//SDRAM IO port
);


assign        CLK_EN    = 1'b1;
assign        nChip_sel = 1'b0;

reg           intLDQM;
reg           intUDQM;
reg [15:0]    DQi;


//State enumeration, 1 hot
parameter
  //Manages refresh interval and latches address/data.
  Idle = 1,       
  
  //Initialization of SDRAM chip
  Init1 = 2,       //Nop
  Init2 = 4,       //Precharge all
  Init3 = 33554432,//Nop
  Init4 = 8,       //Auto refresh
  Init5 = 67108864,//Nop 
  Init6 = 16,      //Set mode
  Init7 = 32,      //Nop
  
  //Auto precharge
  Refresh1 = 64, 
  Refresh2 = 128, 
  Refresh3 = 256, 
  Refresh4 = 512, 
  Refresh5 = 1024, 
  Refresh6 = 2048, 
  Refresh7 = 4096, 
  
  //Single read operation with auto precharge
  Read1 = 8192, 
  Read2 = 16384,  
  Read3 = 32768, 
  Read4 = 65536, 
  Read5 = 131072, 
  Read6 = 262144, 
  Read7 = 524288, 
  
  //Single write operation with auto precharge
  Write1 = 1048576, 
  Write2 = 2097152, 
  Write3 = 4194304, 
  Write4 = 8388608, 
  Write5 = 16777216;

reg  [26:0] state;
reg  [26:0] nextState;
reg			intWE;
reg			intStart;
reg	 [15:0]	initCounter;
reg  [24:0] refreshCounter;
reg  [19:0] internalAddress;
reg  [15:0] internalData;
assign DQ = (state == Write5) ? DQi : 16'bZZZZZZZZZZZZZZZZ;

//Output logic
always @(posedge SDCLK) begin
  if(!nReset) begin
	  DataOut         <= 0;
    Ack             <= 1'b0;
    state           <= Init1;
    initCounter     <= 16'b0;
    refreshCounter  <= 24'b0;
    internalAddress <= 20'b0;
    internalData    <= 0;    
    intUDQM         <= 1'b0;
    intLDQM         <= 1'b0;
  end else begin
    state           <= nextState;
    refreshCounter <= refreshCounter + 1'b1;
    initCounter <= initCounter + 1'b1;
  case (nextState)
    Idle: //Nop
    begin
      internalAddress <= Address;
      internalData <= DataIn;
      intLDQM <= niLDQM;
      intUDQM <= niUDQM;
		intStart  <= Start;
		intWE		 <=WriteEnable;
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    
    /*Initialization is as follows:
    Hold CKE and DQN high, and Nop for 20,000 cycles. (200us)
    Issue a precharge all command, and Nop for 2 cycles. (20ns)
    Issue 9 auto refresh commands.
    Set the mode register.
    Nop for 2 cycles. (20ns)
    */
    Init1://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Init2://Precharge All
    begin
      n_RAS     <= 1'b0;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b0;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 512;  //Set A10 for auto
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Init3://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Init4://Auto Refresh
    begin
      n_RAS     <= 1'b0;
      n_CAS     <= 1'b0;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care 
      ADDRE     <= 0;    //Don't Care    
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Init5://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Init6://Set mode register
    begin
      n_RAS     <= 1'b0;
      n_CAS     <= 1'b0;
      nWriteEN  <= 1'b0;
      BA        <= 2'b0; 
      ADDRE     <= 12'b001000100000; //CL2, Single R/W
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Init7://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    
    Refresh1://Refresh must also latch in any R/W commands.
    begin      
		if(Start == 1'b1) begin
			internalAddress <= Address;
			internalData <= DataIn;
			intLDQM <= niLDQM;
			intUDQM <= niUDQM;
			intStart <= 1'b1;
			intWE <= WriteEnable;
		end
      refreshCounter <= 24'b0;
      n_RAS     <= 1'b0;
      n_CAS     <= 1'b0;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care 
      ADDRE     <= 0;    //Don't Care    
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Refresh2://Nop
    begin      
		if(Start == 1'b1) begin
			internalAddress <= Address;
			internalData <= DataIn;
			intLDQM <= niLDQM;
			intUDQM <= niUDQM;
			intStart <= 1'b1;
			intWE <= WriteEnable;
		end
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Refresh3://Nop
    begin      
		if(Start == 1'b1) begin
			internalAddress <= Address;
			internalData <= DataIn;
			intLDQM <= niLDQM;
			intUDQM <= niUDQM;
			intStart <= 1'b1;
			intWE <= WriteEnable;
		end
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Refresh4://Nop
    begin      
		if(Start == 1'b1) begin
			internalAddress <= Address;
			internalData <= DataIn;
			intLDQM <= niLDQM;
			intUDQM <= niUDQM;
			intStart <= 1'b1;
			intWE <= WriteEnable;
		end
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Refresh5://Nop
    begin      
		if (Start == 1'b1) begin
			internalAddress <= Address;
			internalData <= DataIn;
			intLDQM <= niLDQM;
			intUDQM <= niUDQM;
			intStart <= 1'b1;
			intWE <= WriteEnable;
		end
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Refresh6://Nop
    begin      
		if(Start == 1'b1) begin
			internalAddress <= Address;
			internalData <= DataIn;
			intLDQM <= niLDQM;
			intUDQM <= niUDQM;
			intStart <= 1'b1;
			intWE <= WriteEnable;
		end
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Refresh7://Nop
    begin      
		if(Start == 1'b1) begin
			internalAddress <= Address;
			internalData <= DataIn;
			intLDQM <= niLDQM;
			intUDQM <= niUDQM;
			intStart <= 1'b1;
			intWE <= WriteEnable;
		end
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    
    Write1://Activate Row
	 
    begin
      n_RAS     <= 1'b0;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Always zero for now, only using one bank 
      ADDRE     <= internalAddress[19:9];    
      nLDQM     <= intLDQM;
      nUDQM     <= intUDQM;
      DQi       <= DQi;
      Ack       <= 1'b1; //Aknowledge so CPU can resume
      DataOut   <= DataOut;
    end
    Write2://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b1; //Hold Ack for two cycles to cross clock domain
      DataOut   <= DataOut;
    end
    Write3://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= internalData;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Write4://Write with Auto Precharge
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b0;
      nWriteEN  <= 1'b0;
      BA        <= 2'b0; //Always zero for now, only using one bank 
      ADDRE     <= {3'b010, internalAddress[8:0]};  //Set A10   
      nLDQM     <= intLDQM;
      nUDQM     <= intUDQM;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Write5://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    
    Read1://Activate Row
    begin
      n_RAS     <= 1'b0;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Always zero for now, only using one bank 
      ADDRE     <= internalAddress[19:9];    
      nLDQM     <= intLDQM;
      nUDQM     <= intUDQM;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Read2://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Read3://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Read4://Read with Auto Precharge
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b0;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Always zero for now, only using one bank 
      ADDRE     <= {3'b010, internalAddress[8:0]};    
      nLDQM     <= intLDQM;
      nUDQM     <= intUDQM;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Read5://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b1;
      nUDQM     <= 1'b1;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
    Read6://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DQ;//DataOut
    end
    Read7://Nop, latch, and aknowledge
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b0; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b1;
      DataOut   <= DataOut;//DQ;
    end
    
    default://Nop
    begin
      n_RAS     <= 1'b1;
      n_CAS     <= 1'b1;
      nWriteEN  <= 1'b1;
      BA        <= 2'b1; //Don't Care
      ADDRE     <= 0;    //Don't Care
      nLDQM     <= 1'b0;
      nUDQM     <= 1'b0;
      DQi       <= DQi;
      Ack       <= 1'b0;
      DataOut   <= DataOut;
    end
      
    
  endcase
  end
end

//State logic
always @(*) begin
  nextState = state;
    
case (state) 
  /*Initialization is as follows:
  Hold CKE and DQN high, and Nop for 20,000 cycles. (200us)
  Issue a precharge all command, and Nop for 2 cycles. (20ns)
  Issue 9 auto refresh commands. (980 ns)
  Set the mode register. (20 ns)
  Nop for 2 cycles. (20ns)
  Total: 201040 ns
  */

  Init1: //Nop for 20,000 cycles
    if (initCounter < 20000) begin
    end else begin
      nextState = Init2;
    end 
  Init2: //Precharge all
    if (initCounter < 20001) begin
    end  else begin
      nextState = Init3;
    end
  Init3: //Nop for two cycles
    if (initCounter < 20003) begin
    end else begin
      nextState = Init4;
    end
  Init4: //9 auto refreshes, 49 cycles
    if (initCounter < 20062) begin
    end else begin
      nextState = Init5;
    end
  Init5: //Nop for 5 cycles
    if (initCounter <= 20067) begin
    end else begin
      nextState = Init6;
    end
  Init6: //set mode register
    nextState = Init7;
  Init7: //Nop for two cycles
    if (initCounter <= 20069) begin
    end else begin
      nextState = Idle;
    end
  Idle: begin //Refresh and latch inputs

    if (refreshCounter >= 1540) begin
      nextState = Refresh1;
    end else if (intStart == 1'b1) begin
      if (intWE == 1'b1) begin  //Check polarity
        nextState = Write1;
      end else begin
        nextState = Read1;
      end 
	 end else begin
		 nextState = Idle;
	 end
	end
  //Auto precharge
  Refresh1: nextState = Refresh2;
  Refresh2: nextState = Refresh3;
  Refresh3: nextState = Refresh4;
  Refresh4: nextState = Refresh5;
  Refresh5: nextState = Refresh6;
  Refresh6: nextState = Refresh7;
  Refresh7: begin 
	if (intStart == 1'b1) begin
      if (intWE == 1'b1) begin  //Check polarity
        nextState = Write1;
      end else begin
        nextState = Read1;
      end 
	 end else begin
		nextState = Idle;
    end
	end
  
  //Single read operation with auto precharge
  Read1: nextState = Read2;
  Read2: nextState = Read3;
  Read3: nextState = Read4;
  Read4: nextState = Read5;
  Read5: nextState = Read6;
  Read6: nextState = Read7;
  Read7: nextState = Idle;
  //Single write operation with auto precharge
  Write1: nextState = Write2;
  Write2: nextState = Write3;
  Write3: nextState = Write4;
  Write4: nextState = Write5;
  Write5: nextState = Idle;
  default:nextState = Idle; //Go to Idle
  
endcase
end
endmodule
