//synopsys translate on
module CPU(
  input wire SysClk,
  input wire nReset,
  input wire Interrupt,
  input wire DataReady, //Ack signal for both read and write
  input wire[7:0] InterruptIndex,
  input wire[15:0] DataIn,
  
  output reg[15:0] DataOut,
  output wire[19:0] AddressOut,
  output reg WordByte, //Word = 1, byte = 0
  output reg WriteEnable,
  output reg StartTransaction
);

`include "../CPU/8086.vh"
`include "../CPU/cpu.vh"

////Segment Registers and FLAGS
 reg [15:0] segregs[0:3];
 reg [15:0] FLAGS;


////General purpose registerfile and signals, and real address calculation
reg  gp_we; //GP Register Write Enable
reg  [3:0]  gp_waddr;
reg  [3:0]  gp_raddr;
reg  [15:0] gp_din;
wire [15:0] gp_dout;

wire [15:0] SegReg;//Selects actual segment register for address calculation
reg  [`R_M]  iSegReg; //Instructions and fetch set this (instruction seg reg)
reg  [1:0]  MOD;
reg  [`R_M]  R_M;
reg  PushPop;
reg  [15:0] aDisplacement;

assign SegReg = 
//(seg_override_en == 1'b0) ? segregs[iSegReg] : 
//(prefix == thing) ? thing :
//(prefix == other_thing) ? other_thing:
//(prefix == other_thing) ? other_thing:
//(prefix == other_thing) ? other_thing:
//(prefix == other_thing) ? other_thing:
//(prefix == other_thing) ? other_thing:
//(prefix == other_thing) ? other_thing:
segregs[iSegReg]; //TODO: Remove this line when the above is used instead (Do it in a case rather than ? :,)
//TODO: Create an always block for seg_override_en based on next state, which disables on specific states and enables by default.


GP_REGISTER_FILE gp_registers(
.SysClk ( SysClk ),
.nReset (nReset),
.WriteEnable (gp_we),
.write_addr (gp_waddr),
.read_addr (gp_raddr),
.DataIn (gp_din),

.MOD (MOD),
.R_M (R_M),
.PushPop (PushPop),
.segment_reg (SegReg),
.DisplacementLo (aDisplacement[7:0]),
.DisplacementHi (aDisplacement[15:8]),
.EffectiveAddress (AddressOut),
.DataOut (gp_dout)
);

////ALU signals
reg alu_en;
reg  [15:0] alu_in1;
reg  [15:0] alu_in2;
reg  [2:0]  alu_op;
wire [5:0]  CAPZSO;
wire [15:0] alu_dout;
ALU uut(
.SysClk   (SysClk),
.Enable   (alu_en),
.WordByte (WordByte),
.In1      (alu_in1),
.In2      (alu_in2),
.Op       (alu_op),
.FLAGS    (FLAGS),
.Out      (alu_dout),
.CAPZSO   (CAPZSO) 
);


////Instruction "Buffer"
reg[7:0] instruction[0:6] /* synthesis keep */;
//Byte 0 is prefix, 1 is op
reg[15:0] IP; //Instruction Pointer

reg seg_override_en; //1 to enable prefix overrides to segment registers



////////Giant state machine =D////////////////////////////

reg[30:0] initcounter;
reg [`num_states - 1:0] state, next /* synthesis keep */;
always @(posedge SysClk, negedge nReset) begin
 if (!nReset) begin
  state <= `num_states'b0;
  state[INIT] <= 1'b1;
 end else begin
  state <= `num_states'b0;
  state <= next;
  end
end

//////////////////////////Next state logic//////////////////
always@(*) begin
  next = `num_states'b0;
  case(1'b1) //synopsys full_case parallel_case
    state[INIT]: begin
      if(initcounter[6] == 1'b1) begin //TODO: Actual init timer value
        next[IDLE] = 1'b1;
      end else  begin
        next[INIT] = 1'b1;
      end
    end
    state[IDLE]: begin
      if(Interrupt == 1'b1) begin
        next[INTHa] = 1'b1;
      end else begin
        next[FETCH1a] = 1'b1;
      end
    end
//---------------------------FETCH---------------------------
`include "../CPU/fetch_next_state.v"
    
///////////////////////////DECODE LOGIC/////////////////////
    state[DECODE]: begin
      casex(instruction[1])
        `NOP:
          next[IDLE] = 1'b1;
        `MOVI2R:
          next[IDLE] = 1'b1;
        `MOVRM2SEG:
          case(instruction[2][`MOD])
            2'b11: //register mode
              next[MOVR2SEGa] = 1'b1;
            default:
              next[MOVM2SEGa] = 1'b1;
            endcase
        `MOVW2M:
          next[MOVW2Ma] = 1'b1;
        `MOVB2M:
          next[MOVB2Ma] = 1'b1;
        `MOVM2A:
          next[MOVM2Aa] = 1'b1;
        `MOVA2M:
          next[MOVA2Ma] = 1'b1;
        `MOVR2RM:
          case(instruction[2][`MOD])
            2'b11: //register mode
              next[MOVR2Ra] = 1'b1;
            default:
              next[MOVR2Ma] = 1'b1;
          endcase
        `MOVRM2R:
          case(instruction[2][`MOD])
            2'b11: //register mode
              next[MOVR2Ra] = 1'b1;
            default:
              next[MOVM2Ra] = 1'b1;
          endcase
        `MOVSEG2RM:
          if(instruction[2][`MOD] == 2'b11) begin //register mode or not
            next[IDLE] <= 1'b1;
          end else begin
            next[MOVSEG2M] <= 1'b1;
          end
        `PUSHPOPR: 
          if(instruction[1][3] == 1'b0) begin //Push
            next[PUSHRa] <= 1'b1;
          end else begin //Pop
            next[POPRa] <= 1'b1;
          end
        `ALUR2RM:
          if(instruction[2][`MOD] == 2'b11) begin
            next[ALUR2Ra] <= 1'b1;
          end else begin
            next[ALURMa] <= 1'b1;
          end
        `ALUACC: begin
          next[ALUACCa] <= 1'b1;
         end
          

        default: //halt
          next[HALT] <= 1'b1;
      endcase
    end
////////////////////ADDITIONAL INSTRUCTION LOGIC/////////////////////
    `include "../CPU/mov_next_state.v"




    state[PUSHRa]: begin
      next[PUSHRb] <= 1'b1;
    end

    state[PUSHRb]:begin
      next[PUSHRc] <= 1'b1;
    end

    state[PUSHRc]:begin
      next[PUSHRd] <= 1'b1;
    end

    state[PUSHRd]:begin
      if(!DataReady) begin
        next[PUSHRd] = 1'b1;
      end else begin
        next[IDLE] = 1'b1;
      end              
    end

    state[POPRa]:begin
      if(!DataReady) begin
        next[POPRa] = 1'b1;
      end else begin
        next[POPRb] = 1'b1;
      end              
    end

    state[POPRb]: begin
      next[POPRc] <= 1'b1;
    end

    state[POPRc]: begin
      next[POPRd] <= 1'b1;
    end

    state[POPRd]: begin
      next[IDLE] <= 1'b1;
    end

    state[ALURMa]: begin
      if(!DataReady) begin
        next[ALURMa] <= 1'b1;
      end else begin
        next[ALURMb] <= 1'b1;
      end
    end

    state[ALURMb]: begin
      next[ALURMc] <= 1'b1;
    end

    state[ALURMc]: begin
      next[ALURMd] <= 1'b1;
    end

    state[ALURMd]: begin
      next[ALURMe] <= 1'b1;
    end

    state[ALURMe]: begin
      if(!DataReady && instruction[1][1] == 1'b0) begin
        next[ALURMe] <= 1'b1;
      end else begin
        next[IDLE] <= 1'b1;
      end
    end

    state[ALUR2Ra]: begin
      next[ALUR2Rb] <= 1'b1;
    end

    state[ALUR2Rb]: begin
      next[ALUR2Rc] <= 1'b1;
    end
    
    state[ALUR2Rc]: begin
      next[ALUR2Rd] <= 1'b1;
    end
       
    state[ALUR2Rd]: begin
      next[IDLE] <= 1'b1;
    end
        
    state[ALUACCa]: begin
      next[ALUACCb] <= 1'b1;
    end

    state[ALUACCb]: begin
      next[ALUACCc] <= 1'b1;
    end

    state[ALUACCc]: begin
      next[IDLE] <= 1'b1;
    end
 


    default:
      next[HALT] <= 1'b1;
  endcase
end

////////////////////////Output logic//////////////////////
always@(posedge SysClk, negedge nReset) begin
  
  
  if(!nReset) begin
    initcounter <= 31'b0;  
  end else begin  

    
    case(1'b1) //synopsys parallel_case
    next[INIT]: begin
      initcounter <= initcounter + 1'b1;
      iSegReg <= `CS_ADDR;
      WriteEnable <= 1'b0;
      StartTransaction <= 1'b0;
      seg_override_en <= 1'b0;
      IP <= 16'b0;
      segregs[`ES_ADDR] <= 16'b0;
      segregs[`SS_ADDR] <= 16'b0;
      segregs[`DS_ADDR] <= 16'b0;
      segregs[`CS_ADDR] <= 16'hF000; //TODO: This should be FFFF, setting F000 to make Com2MIF easier for now. I guess this could be permanent...
      gp_we <= 1'b0;
      alu_en <= 1'b0;
      FLAGS <= 16'b0;

    end
    
    next[IDLE]: begin
      gp_we <= 1'b0;
      WordByte <= 1'b1;
      instruction[0] <= 8'b0;
      WriteEnable <= 1'b0;
      StartTransaction <= 1'b0;
      alu_en <= 1'b0;
    end
    
    next[HALT]: begin
      DataOut <= 16'b0;
      WriteEnable <= 1'b0;
    end
    
    `include "../CPU/fetch_output.v"
/////////////////////////////////////INSTRUCTIONS///////////////////////////////////////

///////First cycle of every instruction happens in the decode block
      next[DECODE]: begin
      casex(instruction[1])
////////////////////////MOV///////////////////////////////
        `MOVI2R: begin
          gp_waddr <= instruction[1][3:0];
          gp_din <= {instruction[3], instruction[2]};
          gp_we <= 1'b1;
        end
        `MOVRM2SEG:
          case(instruction[2][`MOD])
            2'b11: begin //register mode 
              gp_raddr <= {1'b1, instruction[2][`R_M]}; //Always a word register
              IP <= IP - 2'b10;
            end
            default: begin
              MOD <= instruction[2][`MOD];
              R_M <= instruction[2][`R_M];
              aDisplacement <= {instruction[4], instruction[3]};
              iSegReg <= `DS_ADDR;
              StartTransaction <= 1'b1;
            end
          endcase
        `MOVW2M: begin
          iSegReg <= `DS_ADDR;
          MOD <= instruction[2][`MOD];
          R_M <= instruction[2][`R_M];
          aDisplacement <= {instruction[4], instruction[3]};
          DataOut <= {instruction[6], instruction[5]};
          WriteEnable <= 1'b1;
          StartTransaction <= 1'b1;
        end
        `MOVB2M: begin
          iSegReg <= `DS_ADDR;
          MOD <= instruction[2][`MOD];
          R_M <= instruction[2][`R_M];
          aDisplacement <= {instruction[4], instruction[3]};
          DataOut <= {instruction[6], instruction[5]}; //instruction[6] will be garbage, but ignored
          WordByte <= 1'b0; //Byte transaction          
          WriteEnable <= 1'b1;
          StartTransaction <= 1'b1;
        end
        `MOVM2A: begin //Read value from memory
          MOD <= 2'b0; //Memory mode
          R_M <= 3'd6; //Direct 16-bit displacement
          iSegReg <= `DS_ADDR;
          WordByte <= instruction[1][0];
          aDisplacement <= {instruction[3], instruction[2]};
          WriteEnable <= 1'b0;
          StartTransaction <= 1'b1;
        end
        `MOVA2M: begin //Write accumulator to memory
          iSegReg <= `DS_ADDR;
          MOD <= 2'b0; //Memory mode
          R_M <= 3'd6; //Direct 16-bit displacement
          aDisplacement <= {instruction[3], instruction[2]};
          if(instruction[1][0] == 1'b0) begin
            gp_raddr <= `AL_ADDR;
            WordByte <= 1'b0;
          end else begin
            gp_raddr <= `AX_ADDR;
            WordByte <= 1'b1;
          end
        end
        `MOVR2RM: begin //Write register to register/memory
          iSegReg <= `DS_ADDR;
          MOD <= instruction[2][`MOD];
          R_M <= instruction[2][`R_M];
          aDisplacement <= {instruction[4], instruction[3]};
          WordByte <= instruction[1][0];
          gp_raddr <= {instruction[1][0], instruction[2][`REG]};
          gp_waddr <= {instruction[1][0], instruction[2][`R_M]};
        end
        `MOVRM2R: begin //Write register or memory to register
          iSegReg <= `DS_ADDR;
          MOD <= instruction[2][`MOD];
          R_M <= instruction[2][`R_M];
          aDisplacement <= {instruction[4], instruction[3]};
          WordByte <= instruction[1][0];
          gp_raddr <= {instruction[1][0], instruction[2][`R_M]};
          gp_waddr <= {instruction[1][0], instruction[2][`REG]};
          WriteEnable <= 1'b0;
          if(instruction[2][`MOD] == 2'b11) begin //register mode or not
            StartTransaction <= 1'b0;
          end else begin
            StartTransaction <= 1'b1;
          end
        end
        `MOVSEG2RM: begin//Write segreg to register or memory
          iSegReg <= `DS_ADDR;
          MOD <= instruction[2][`MOD];
          R_M <= instruction[2][`R_M];
          aDisplacement <= {instruction[4], instruction[3]};
          WordByte <= instruction[1][0];
          DataOut <= segregs[instruction[2][`REG]];
          gp_din <= segregs[instruction[2][`REG]];
          gp_waddr <= {1'b1, instruction[2][`R_M]};
          WriteEnable <= 1'b0;
          if(instruction[2][`MOD] == 2'b11) begin //register mode or not
            gp_we <= 1'b1;
            WriteEnable <= 1'b0;
            StartTransaction <= 1'b0;
          end else begin
            gp_we <= 1'b0;
            WriteEnable <= 1'b1;
            StartTransaction <= 1'b1;
          end
        end
        `PUSHPOPR: begin
          MOD <= 2'b00; //Memory mode, no displacement
          iSegReg <= `SS_ADDR;
          R_M <= 3'b100; //Source index is overridden in GP_REGISTER_FILE.v when PushPop == 1'b1
          PushPop <= 1'b1;
          WordByte <= 1'b1; //Always a word
          DataOut <= gp_dout;
          if(instruction[1][3] == 1'b0) begin //Push
            gp_raddr <= `SP_ADDR;
            gp_waddr <= `SP_ADDR;
            StartTransaction <= 1'b0;
            WriteEnable <= 1'b0;
            gp_we <= 1'b0 ;
          end else begin //Pop
            gp_raddr <= {1'b1,instruction[1][`R_M]};
            gp_waddr <= {1'b1,instruction[1][`R_M]};
            gp_din <= DataIn;
            StartTransaction <= 1'b1;
            WriteEnable <= 1'b0;
          end
        end
        `ALUR2RM: begin //Perform ALU op on register and register/memory, store in register/memory
          iSegReg <= `DS_ADDR;
          MOD <= instruction[2][`MOD];
          R_M <= instruction[2][`R_M];
          aDisplacement <= {instruction[4], instruction[3]};
          WordByte <= instruction[1][0];
          gp_raddr <= {instruction[1][0], instruction[2][`REG]}; //All cases read from REG field
          WriteEnable <= 1'b0;
          StartTransaction <= (instruction[2][`MOD] != 2'b11); //If memory is involved, read the memory
          alu_op <= instruction[1][5:3];
        end
        `ALUACC: begin //ALU op on accumulator and immediate
          WordByte <= instruction[1][0];
          iSegReg <= `DS_ADDR;
          alu_op <= instruction[1][5:3];
          gp_raddr <= {instruction[1][0], 3'd0};
          gp_waddr <= {instruction[1][0], 3'd0};
        end
      endcase
    end


    `include "../CPU/mov_output.v"

    next[PUSHRa]: begin
      PushPop <= 1'b1;
      gp_din   <= gp_dout - 2'b10;
      gp_we <= 1'b1;
    end

    next[PUSHRb]: begin
      PushPop <= 1'b1;
      gp_raddr <= {1'b1,instruction[1][`R_M]};
      DataOut <= gp_dout;
      gp_we <= 1'b0;
    end

    next[PUSHRc]: begin
      PushPop <= 1'b1;
      gp_raddr <= {1'b1,instruction[1][`R_M]};
      DataOut <= gp_dout;
      StartTransaction <= 1'b1;
      WriteEnable <= 1'b1;
    end

    next[PUSHRd]: begin
      PushPop <= 1'b1;
      gp_raddr <= {1'b1,instruction[1][`R_M]};
      DataOut <= gp_dout;
      StartTransaction <= 1'b0;
      WriteEnable <= 1'b0;
    end

    next[POPRa]: begin
      StartTransaction <= 1'b0;
    end

    next[POPRb]: begin
      PushPop <= 1'b1;
      gp_we <= 1'b1;
      gp_din <= DataIn;
    end

    next[POPRc]: begin
      PushPop <= 1'b1;
      gp_raddr <= `SP_ADDR;
      gp_waddr <= `SP_ADDR;
    end

    next[POPRd]: begin
      PushPop <= 1'b1;
      gp_raddr <= `SP_ADDR;
      gp_waddr <= `SP_ADDR;
      gp_din   <= gp_dout + 2'b10;
      gp_we <= 1'b1;
    end

    next[ALURMa]: begin //Wait for data to be valid
      StartTransaction <= 1'b0;
    end

    next[ALURMb]: begin 
      if (instruction[1][1] == 1'b1) begin
        alu_in1 <= gp_dout;
        alu_in2 <= DataIn;
      end else begin
        alu_in1 <= DataIn;
        alu_in2 <= gp_dout;
      end
      alu_en <= 1'b1;
    end

    next[ALURMc]: begin
    end

    next[ALURMd]: begin //Write data back to memory or register
      alu_en <= 1'b0;
      if (instruction[1][1] == 1'b1) begin
        gp_waddr <= {instruction[1][0], instruction[2][`REG]};
        gp_we <= 1'b1;
        gp_din <= alu_dout;
      end else begin
        WriteEnable <= 1'b1;
        DataOut <= alu_dout;
        StartTransaction <= 1'b1;
      end
      `WRITE_CAPZSO
    end
    
    next[ALURMe]: begin //This wait for memory ack state could be re-named and re-used
      WriteEnable <= 1'b0;
      StartTransaction <= 1'b0;
    end

    next[ALUR2Ra]: begin //Read other register
      gp_raddr <= {instruction[1][0], instruction[2][`R_M]};
      
      //Direction
      if (instruction[1][1] == 1'b1) begin //op REG, R_M
        alu_in1 <= gp_dout;
        gp_waddr <= {instruction[1][0], instruction[2][`REG]};
      end else begin //op R_M, REG
        gp_waddr <= {instruction[1][0], instruction[2][`R_M]};
        alu_in2 <= gp_dout;
      end
    end

    next[ALUR2Rb]: begin //Enable ALU
      //Direction
      if (instruction[1][1] == 1'b1) begin
        alu_in2 <= gp_dout;
      end else begin
        alu_in1 <= gp_dout;
      end
      alu_en <= 1'b1;
    end

    next[ALUR2Rc]: begin
    end

    next[ALUR2Rd]: begin //Write result
      gp_din <= alu_dout;
      gp_we <= 1'b1;
      alu_en <= 1'b0;
      `WRITE_CAPZSO
    end

    next[ALUACCa]: begin
      alu_in1 <= gp_dout;
      alu_in2 <= {instruction[3], instruction[2]};
      alu_en <= 1'b1;
    end

    next[ALUACCb]: begin
    end

    next[ALUACCc]: begin
      gp_din <= alu_dout;
      gp_we <= 1'b1;
      alu_en <= 1'b0;
      `WRITE_CAPZSO
    end
    

    next[INTHa]: 
      DataOut <= 16'd3;
      //TODO: Handle interrupts

    default:
      DataOut <= 16'd4;
      //TODO: Do a default
    endcase
  end
end


endmodule
