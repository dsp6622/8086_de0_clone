
////////////////////////MOV///////////////////////////////

    next[MOVR2SEGa]: begin
      segregs[instruction[2][5:3]] <= gp_dout;
    end
      
    next[MOVM2SEGa]: begin
      StartTransaction <= 1'b0;
      segregs[instruction[2][5:3]] <= {DataIn[7:0], DataIn[15:8]};
    end
    
    next[MOVW2Ma]: begin
      StartTransaction <= 1'b0;
      WriteEnable <= 1'b0;
    end
    
    next[MOVB2Ma]: begin
      StartTransaction <= 1'b0;
      WriteEnable <= 1'b0;
    end

    next[MOVM2Aa]: begin //Wait for data to be valid
      StartTransaction <= 1'b0;
      WriteEnable <= 1'b0; 

    end

    next[MOVM2Ab]: begin //Store data to registers
      MOD <= 2'b00;
      if(instruction[1][0] == 1'b0) begin
        gp_waddr <= `AL_ADDR;
      end else begin
        gp_waddr <= `AX_ADDR;
      end
      gp_din <= DataIn;
      gp_we <= 1'b1;
    end

    next[MOVA2Ma]: begin
      DataOut <= gp_dout;       
      WriteEnable <= 1'b1;
      StartTransaction <= 1'b1;    
    end
   
    next[MOVA2Mb]: begin  
      WriteEnable <= 1'b0;
      StartTransaction <= 1'b0;    
    end

    next[MOVR2Ra]: begin
      gp_din <= gp_dout;
      gp_we <= 1'b1;
    end
   
    next[MOVR2Ma]: begin
      DataOut <= gp_dout;       
      WriteEnable <= 1'b1;
      StartTransaction <= 1'b1;       
    end

    next[MOVR2Mb]: begin
      WriteEnable <= 1'b0;
      StartTransaction <= 1'b0;
    end

    next[MOVM2Ra]: begin //Wait for data to be valid
      StartTransaction <= 1'b0;
      WriteEnable <= 1'b0; 
    end

    next[MOVM2Rb]: begin
      gp_din <= DataIn;
      gp_we <= 1'b1;  
    end

    next[MOVSEG2M]: begin
      StartTransaction <= 1'b0;
      WriteEnable <= 1'b0;
    end
