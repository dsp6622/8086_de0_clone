/*
Make sure to set the FLAGS register appropriately outside of this
module.

8-bit mode was a bit of an afterthought... Definitely could have made this cleaner
*/

module ALU(
  input  wire SysClk,
  input  wire Enable,
  input  wire WordByte,
  input  wire[15:0] In1,
  input  wire[15:0] In2, 
  input  wire[2:0] Op,
  input  wire[15:0] FLAGS,
  output reg[15:0] Out,
  output reg[5:0]  CAPZSO //Carry, Adj, Parity, Zero, Sign, Overflow
);

/*
Op comes from the second instruction byte's middle
three bits for ALU instructions,
000 ADD
001 OR 
010 ADC
011 SBB
100 AND
101 SUB
110 XOR
111 CMP

*/


reg[16:0] Out_Extended;
wire[15:0] a;
wire[15:0] b; 
assign a = WordByte ? In1 : {8'b0,In1[7:0]};
assign b = WordByte ? In2 : {8'b0,In2[7:0]};


always@(posedge SysClk) begin
  if(Enable == 1'b1) begin
    //Defaults      
    CAPZSO <= {FLAGS[0],FLAGS[4],FLAGS[2],FLAGS[6],FLAGS[7],FLAGS[11]};
    if(WordByte == 1'b1) begin
      case(Op)
      3'd0: begin //ADD
        Out_Extended = {1'b0,a} + {1'b0,b};
        Out <= Out_Extended[15:0];
        CAPZSO[0] <= (a[15] & b[15] & ~ Out_Extended[15]) | (~a[15] & ~b[15] & Out_Extended[15]);
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[16];
      end

      3'd1: begin //OR
        Out_Extended = {1'b0, a | b};
        Out <= Out_Extended[15:0];
        CAPZSO[0] <= 1'b0;
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[5] <= 1'b0;        
      end

      3'd2: begin //ADC
        Out_Extended = {1'b0,a} + {1'b0,b} + FLAGS[0];
        Out <= Out_Extended[15:0];
        CAPZSO[0] <= (a[15] & b[15] & ~ Out_Extended[15]) | (~a[15] & ~b[15] & Out_Extended[15]);
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[16];
      end

      3'd3: begin //SBB
        Out_Extended = {1'b0,a} + ~{1'b0,b} + 1'b1 - FLAGS[0];
        Out <= Out_Extended[15:0];
        CAPZSO[0] <= (a[15] & ~b[15] & ~ Out_Extended[15]) | (~a[15] & b[15] & Out_Extended[15]);
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[16];      
      end

      3'd4: begin //AND
        Out_Extended = {1'b0, a & b};
        Out <= Out_Extended[15:0];
        CAPZSO[0] <= 1'b0;
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[5] <= 1'b0;     
      end

      3'd5: begin //SUB
        Out_Extended = {1'b0,a} + ~{1'b0,b} + 1'b1;
        Out <= Out_Extended[15:0];
        CAPZSO[0] <= (a[15] & ~b[15] & ~ Out_Extended[15]) | (~a[15] & b[15] & Out_Extended[15]);
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[16];        
      end

      3'd6: begin //XOR
        Out_Extended = {1'b0, a ^ b};
        Out <= Out_Extended[15:0];
        CAPZSO[0] <= 1'b0;
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[5] <= 1'b0;     
      end

      3'd7: begin //CMP
        Out_Extended = {1'b0,a} + ~{1'b0,b} + 1'b1;
        CAPZSO[0] <= (a[15] & ~b[15] & ~ Out_Extended[15]) | (~a[15] & b[15] & Out_Extended[15]);
        CAPZSO[1] <= Out_Extended[15];
        CAPZSO[2] <= Out_Extended[15:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[16];            
      end
      endcase
    end else begin
      case(Op)
      3'd0: begin //ADD
        Out_Extended = {1'b0,a} + {1'b0,b};
        Out <= {8'b0,Out_Extended[7:0]};
        CAPZSO[0] <= (a[7] & b[7] & ~ Out_Extended[7]) | (~a[7] & ~b[7] & Out_Extended[7]);
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 8'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[8];
      end

      3'd1: begin //OR
        Out_Extended = {1'b0, a | b};
        Out <= {8'b0,Out_Extended[7:0]};
        CAPZSO[0] <= 1'b0;
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 8'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[5] <= 1'b0;        
      end

      3'd2: begin //ADC
        Out_Extended = {1'b0,a} + {1'b0,b} + FLAGS[0];
        Out <= {8'b0,Out_Extended[7:0]};
        CAPZSO[0] <= (a[7] & b[7] & ~ Out_Extended[7]) | (~a[7] & ~b[7] & Out_Extended[7]);
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 16'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[8];
      end

      3'd3: begin //SBB
        Out_Extended = {1'b0,a} + ~{1'b0,b} + 1'b1 - FLAGS[0];
        Out <= {8'b0,Out_Extended[7:0]};
        CAPZSO[0] <= (a[7] & ~b[7] & ~ Out_Extended[7]) | (~a[7] & b[7] & Out_Extended[7]);
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 8'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[8];      
      end

      3'd4: begin //AND
        Out_Extended = {1'b0, a & b};
        Out <= {8'b0,Out_Extended[7:0]};
        CAPZSO[0] <= 1'b0;
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 8'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[5] <= 1'b0;     
      end

      3'd5: begin //SUB
        Out_Extended = {1'b0,a} + ~{1'b0,b} + 1'b1;
        Out <= {8'b0,Out_Extended[7:0]};
        CAPZSO[0] <= (a[7] & ~b[7] & ~ Out_Extended[7]) | (~a[7] & b[7] & Out_Extended[7]);
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 8'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[8];        
      end

      3'd6: begin //XOR
        Out_Extended = {1'b0, a ^ b};
        Out <= {8'b0,Out_Extended[7:0]};
        CAPZSO[0] <= 1'b0;
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 8'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[5] <= 1'b0;     
      end

      3'd7: begin //CMP
        Out_Extended = {1'b0,a} + ~{1'b0,b} + 1'b1;
        CAPZSO[0] <= (a[7] & ~b[7] & ~ Out_Extended[7]) | (~a[7] & b[7] & Out_Extended[7]);
        CAPZSO[1] <= Out_Extended[7];
        CAPZSO[2] <= Out_Extended[7:0] == 8'd0;
        CAPZSO[3] <= ~^Out_Extended[7:0];
        CAPZSO[4] <= ({1'b0,a[7:0]} + {1'b0,b[7:0]}) > 9'hFF;
        CAPZSO[5] <= Out_Extended[8];            
      end
      endcase
    end
  end
end
endmodule
