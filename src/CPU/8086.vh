`ifndef _8086_vh__
`define _8086_vh__


/*     ereg  reg
000    AX    AL
001    CX    CL
010    DX    DL
011    BX    BL
100    SP    AH
101    BP    CH
110    SI    DH
111    DI    BH

000 ES
001 CS
010 SS
011 DS
*/


// Register addresses
`define AL_ADDR 4'b0000
`define BL_ADDR 4'b0011
`define CL_ADDR 4'b0001
`define DL_ADDR 4'b0010
`define AH_ADDR 4'b0100
`define BH_ADDR 4'b0111
`define CH_ADDR 4'b0101
`define DH_ADDR 4'b0110
`define AX_ADDR 4'b1000
`define BX_ADDR 4'b1011
`define CX_ADDR 4'b1001
`define DX_ADDR 4'b1010
`define SP_ADDR 4'b1100
`define BP_ADDR 4'b1111
`define SI_ADDR 4'b1101
`define DI_ADDR 4'b1110

`define ES_ADDR 3'b000
`define CS_ADDR 3'b001
`define SS_ADDR 3'b010
`define DS_ADDR 3'b011

/* Direction bit: 
   0: MOD R/M <= REG
   1: REG <= MOD R/M
*/

`define MOD 7:6
`define REG 5:3
`define R_M 2:0

`endif
