
/* MOD
00 - Memory mode, no displacement (except when R/M == 110, then direct 16-bit displacement)
01 - 8 bit displacement
10 - 16 bit displacement
11 - Register mode
*/

/* R_M
See Intel docs or case statement below
*/

/* Direction bit: 
   0: MOD R/M <= REG
   1: REG <= MOD R/M
*/

//Synchronous write, asynchronous read
//Real address calculation
`include "../CPU/8086.vh"
module GP_REGISTER_FILE(
  //Register File
  input wire SysClk,
  input wire nReset,
  input wire WriteEnable,
  input wire[3:0] write_addr,
  input wire[3:0] read_addr,
  input wire[15:0] DataIn,
  //Address Calculation
  input wire[1:0] MOD,
  input wire[2:0] R_M,
  input wire      PushPop,//0 for none, 1 for push/pop

  input wire[15:0] segment_reg,
  input wire[7:0] DisplacementLo,
  input wire[7:0] DisplacementHi,
  
  output wire[19:0] EffectiveAddress,
  output wire[15:0] DataOut
);


//Register File
reg[15:0] AX;
reg[15:0] CX;
reg[15:0] DX;
reg[15:0] BX;
reg[15:0] SP;
reg[15:0] BP;
reg[15:0] SI;
reg[15:0] DI;

assign DataOut = 
  (read_addr == `AL_ADDR) ? {8'd0, AX[7:0]} :
  (read_addr == `CL_ADDR) ? {8'd0, CX[7:0]} :
  (read_addr == `DL_ADDR) ? {8'd0, DX[7:0]} :
  (read_addr == `BL_ADDR) ? {8'd0, BX[7:0]} :
  (read_addr == `AH_ADDR) ? {8'd0, AX[15:8]} :
  (read_addr == `CH_ADDR) ? {8'd0, CX[15:8]} :
  (read_addr == `DH_ADDR) ? {8'd0, DX[15:8]} :
  (read_addr == `BH_ADDR) ? {8'd0, BX[15:8]} :
  (read_addr == `AX_ADDR) ? AX :
  (read_addr == `CX_ADDR) ? CX :
  (read_addr == `DX_ADDR) ? DX :
  (read_addr == `BX_ADDR) ? BX :
  (read_addr == `SP_ADDR) ? SP :
  (read_addr == `BP_ADDR) ? BP :
  (read_addr == `SI_ADDR) ? SI :
  DI;
  
always@(posedge SysClk) begin
  if(!nReset) begin
    AX <= 16'd0;
    BX <= 16'd0;
    CX <= 16'd0;
    DX <= 16'd0;
    SP <= 16'd0;
    BP <= 16'd0;
    SI <= 16'd0;
    DI <= 16'd0;//TODO: Find out actual defaults
  end else if(WriteEnable == 1'b1) begin
    AX <= AX;
    BX <= BX;
    CX <= CX;
    DX <= DX;
    SP <= SP;
    BP <= BP;
    SI <= SI;
    DI <= DI;
    case(write_addr)
      `AL_ADDR: AX <= {AX[15:8], DataIn[7:0]};
      `CL_ADDR: CX <= {CX[15:8], DataIn[7:0]};
      `DL_ADDR: DX <= {DX[15:8], DataIn[7:0]};
      `BL_ADDR: BX <= {BX[15:8], DataIn[7:0]};
      `AH_ADDR: AX <= {DataIn[7:0], AX[7:0]};
      `CH_ADDR: CX <= {DataIn[7:0], CX[7:0]};
      `DH_ADDR: DX <= {DataIn[7:0], DX[7:0]};
      `BH_ADDR: BX <= {DataIn[7:0], BX[7:0]};
      `AX_ADDR: AX <= DataIn;
      `CX_ADDR: CX <= DataIn;
      `DX_ADDR: DX <= DataIn;
      `BX_ADDR: BX <= DataIn;
      `SP_ADDR: SP <= DataIn;
      `BP_ADDR: BP <= DataIn;
      `SI_ADDR: SI <= DataIn;
      default: DI <= DataIn;
    endcase
  end else begin
  
    AX <= AX;
    BX <= BX;
    CX <= CX;
    DX <= DX;
    SP <= SP;
    BP <= BP;
    SI <= SI;
    DI <= DI;
	 end
end


//Address Calculation

reg[15:0] index_intermediate;
reg[15:0] displacement_intermediate;
assign EffectiveAddress = {4'b0, displacement_intermediate} + {segment_reg, 4'b0};

always@(*) begin

case(MOD)
	0: displacement_intermediate = index_intermediate;
	1: displacement_intermediate = index_intermediate + {8'd0, DisplacementLo};
	2: displacement_intermediate = index_intermediate + {DisplacementHi, DisplacementLo};
	default: displacement_intermediate = 16'd0;
endcase 
end

always@(*) begin

case(R_M)
	0: index_intermediate = BX + SI;
	1: index_intermediate = BX + DI;
	2: index_intermediate = BP + SI;
	3: index_intermediate = BP + DI;
	4: begin
    case(PushPop)
      0: index_intermediate = SI;
      1: index_intermediate = PushPop ? SP : SI;
    endcase
  end
	5: index_intermediate = DI;
	6: index_intermediate = (MOD[0] || MOD[1]) ? BP : {DisplacementHi, DisplacementLo}; //Also used for instruction fetch
	7: index_intermediate = BX;
	default: index_intermediate = BX;
endcase
end


endmodule
