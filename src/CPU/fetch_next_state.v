    state[FETCH1a]: begin
      next[FETCH1b] = 1'b1;
    end
    state[FETCH1b]: begin
      if(!DataReady) begin
        next[FETCH1b] = 1'b1;
      end else begin
        next[FETCH1c] = 1'b1;
      end
    end
    
    state[FETCH1c]: begin
      if(instruction[0] == 8'b0) begin //no prefix
        casex(instruction[1]) //Check number of operands to read
               `ONE_BYTE_OPS, `TWO_BYTE_OPS: 
                 next[DECODE] = 1'b1;
               default:
                 next[FETCH2a] = 1'b1;        
        endcase
      end else begin //prefix
        casex(instruction[1]) //Check number of operands to read
               `ONE_BYTE_OPS: 
                 next[DECODE] = 1'b1;
               default:
                 next[FETCH2a] = 1'b1;        
        endcase
      end
    end
    
    state[FETCH2a]: begin
      next[FETCH2b] = 1'b1;
    end
    state[FETCH2b]: begin
      if(!DataReady) begin
        next[FETCH2b] = 1'b1;
      end else begin
        next[FETCH2c] = 1'b1;
      end
    end
    
    state[FETCH2c]: begin
      if(instruction[0] == 8'b0) begin //no prefix
        casex(instruction[1]) //Check number of operands to read
             `THREE_BYTE_OPS, `FOUR_BYTE_OPS:
                 next[DECODE] = 1'b1;
               default:
                 next[FETCH3a] = 1'b1;        
        endcase
      end else begin //prefix
        casex(instruction[1]) //Check number of operands to read
               `TWO_BYTE_OPS, `THREE_BYTE_OPS: 
                 next[DECODE] = 1'b1;
               default:
                 next[FETCH3a] = 1'b1;        
        endcase
      end
    end
    
    state[FETCH3a]: begin
      next[FETCH3b] = 1'b1;
    end
    state[FETCH3b]: begin
      if(!DataReady) begin
        next[FETCH3b] = 1'b1;
      end else begin
        next[FETCH3c] = 1'b1;
      end
    end
    
    state[FETCH3c]: begin
      if(instruction[0] == 8'b0) begin //no prefix
               next[DECODE] = 1'b1;
      end else begin //prefix
        casex(instruction[1]) //Check number of operands to read
               `FOUR_BYTE_OPS, `FIVE_BYTE_OPS: 
                 next[DECODE] = 1'b1;
               default:
                 next[FETCH4a] = 1'b1;        
        endcase
      end
    end
    
    state[FETCH4a]: begin
      next[FETCH4b] = 1'b1;
    end
    state[FETCH4b]: begin
      if(!DataReady) begin
        next[FETCH4b] = 1'b1;
      end else begin
        next[FETCH4c] = 1'b1;
      end
    end
    
    state[FETCH3c]: begin
            next[DECODE] = 1'b1;
    end
     
     state[HALT]: begin
        next[HALT] = 1'b1;
    end