next[FETCH1a]: begin
      //Fetch word from instruction memory
      aDisplacement <= IP;
      iSegReg <= `CS_ADDR;
      MOD <= 2'b0;
      R_M <= 3'd6; //Direct displacement
      StartTransaction <= 1'b1;
    end
next[FETCH1b]: begin
    StartTransaction <= 1'b0;
end
next[FETCH1c]: begin
  casex(DataIn[7:0]) //Prefix or op-code
    `PREFIXES: begin
        instruction[0] <= DataIn[7:0]; //Prefix 
        instruction[1] <= DataIn[15:8];  //Op
      IP <= IP + 2'b10;
    end
    default: begin
        instruction[1] <= DataIn[7:0]; //Op
        instruction[2] <= DataIn[15:8];  //Mod/RM
        //Need to deal with variable length ops.....

      casex(DataIn[7:0]) //Calculate how much to increment
        `MOD_DEPENDENT_OPS: //This won't work for 1 byte ops. In fact, this might only work for MOVR2RM.
          case(DataIn[15:14])
          2'b11:
            IP <= IP; //Normally would add two, but don't since this is a register operation

          default:
            IP <= IP + 2'b10; //Add bytes for address
          endcase

        `ONE_BYTE_OPS: 
          IP <= IP + 2'b01; //Second byte is the next instruction and will be re-loaded on the next fetch
        default:
          IP <= IP + 2'b10; //Second byte will be used in this instruction
      endcase
    end
    endcase
end
      
next[FETCH2a]: begin
  //Fetch word from instruction memory
  aDisplacement <= IP;
  iSegReg <= `CS_ADDR;
  MOD <= 2'b0;
  R_M <= 3'd6; //Direct displacement
  StartTransaction <= 1'b1;
end
  next[FETCH2b]: begin
      StartTransaction <= 1'b0;
  end
next[FETCH2c]: begin
  if(instruction[0] == 8'b0) begin //no prefix
    instruction[3] <= DataIn[7:0];
    instruction[4] <= DataIn[15:8];
    casex(instruction[1])
      `THREE_BYTE_OPS:
       IP <= IP + 2'b01;
      default:
       IP <= IP + 2'b10;
    endcase
  end else begin
    instruction[2] <= DataIn[7:0];
    instruction[3] <= DataIn[15:8];
    casex(instruction[1])
      `TWO_BYTE_OPS:
       IP <= IP + 2'b01;
      default:
       IP <= IP + 2'b10;
    endcase
  end
end
  
next[FETCH3a]: begin
  //Fetch word from instruction memory
  aDisplacement <= IP;
  iSegReg <= `CS_ADDR;
  MOD <= 2'b0;
  R_M <= 3'd6; //Direct displacement
  StartTransaction <= 1'b1;
end
  next[FETCH3b]: begin
      StartTransaction <= 1'b0;
  end
next[FETCH3c]: begin
  if(instruction[0] == 8'b0) begin //no prefix
    instruction[5] <= DataIn[7:0];
    instruction[6] <= DataIn[15:8];
    casex(instruction[1])
      `FIVE_BYTE_OPS:
       IP <= IP + 2'b01;
      default:
       IP <= IP + 2'b10;
    endcase
  end else begin
    instruction[4] <= DataIn[7:0];
    instruction[5] <= DataIn[15:8];
    casex(instruction[1])
      `FOUR_BYTE_OPS:
       IP <= IP + 2'b01;
      default:
       IP <= IP + 2'b10;
    endcase
  end
end 
  
next[FETCH4a]: begin
  //Fetch word from instruction memory
  aDisplacement <= IP;
  iSegReg <= `CS_ADDR;
  MOD <= 2'b0;
  R_M <= 3'd6; //Direct displacement
  StartTransaction <= 1'b1;
end
next[FETCH4b]: begin
    StartTransaction <= 1'b0;
end
next[FETCH4c]: begin //Only way to get here is with a prefix and a six byte instruction
  instruction[6] <= DataIn[7:0];
  IP <= IP + 2'b01;
end
