    state[MOVR2SEGa]:
      next[IDLE] = 1'b1;
    state[MOVM2SEGa]: begin
      if(!DataReady) begin
        next[MOVM2SEGa] = 1'b1;
      end else begin
        next[IDLE] = 1'b1;
      end
    end
    state[MOVW2Ma]:begin
      if(!DataReady) begin
        next[MOVW2Ma] = 1'b1;
      end else begin
        next[IDLE] = 1'b1;
      end
    end
    
    state[MOVB2Ma]:begin
      if(!DataReady) begin
        next[MOVB2Ma] = 1'b1;
      end else begin
        next[IDLE] = 1'b1;
      end
    end

    state[MOVM2Aa]: begin
      if(!DataReady) begin
        next[MOVM2Aa] = 1'b1;
      end else begin
        next[MOVM2Ab] = 1'b1;
      end        
    end

    state[MOVM2Ab]: begin
        next[IDLE] = 1'b1;
    end

    state[MOVA2Ma]: begin
      next[MOVA2Mb] = 1'b1; 
    end

    state[MOVA2Mb]: begin
        if(!DataReady) begin
            next[MOVA2Mb] = 1'b1;
        end else begin
            next[IDLE] = 1'b1;
        end
    end

    state[MOVR2Ra]: begin
        next[IDLE] = 1'b1;
    end


    state[MOVR2Ma]: begin
        next[MOVR2Mb] = 1'b1;
    end

    state[MOVR2Mb]: begin
        if(!DataReady) begin
            next[MOVR2Mb] = 1'b1;
        end else begin
            next[IDLE] = 1'b1;
        end

    end

    state[MOVM2Ra]: begin
      if(!DataReady) begin
        next[MOVM2Ra] = 1'b1;
      end else begin
        next[MOVM2Rb] = 1'b1;
      end              
    end

    state[MOVM2Rb]: begin
      next[IDLE] = 1'b1;
    end

    state[MOVSEG2M]:begin
      if(!DataReady) begin
        next[MOVSEG2M] = 1'b1;
      end else begin
        next[IDLE] = 1'b1;
      end              
    end
