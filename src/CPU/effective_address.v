/*
MOD 00 -> Memory mode, no displacement
MOD 01 -> Memory mode, 8-bit displacement
MOD 10 -> Memory mode, 16-bit displacement
MOD 11 -> Register mode, don't care
*/


module AddressCalculate(
input wire[1:0] MOD,
input wire[2:0] R_M,
input wire[7:0] prefix,
input wire[15:0] BX,
input wire[15:0] BP,
input wire[15:0] SI,
input wire[15:0] DI,
input wire[15:0] SS,
input wire[15:0] ES,
input wire[15:0] DS,
input wire[15:0] CS,
input wire[7:0] DisplacementLo,
input wire[7:0] DisplacementHi,
input wire[2:0] Segment,
output wire[19:0] EffectiveAddress
);

reg[15:0] index_intermediate;
reg[15:0] displacement_intermediate;
assign EffectiveAddress = 
  (Segment == 3'b000) ? {4'b0, displacement_intermediate} + {ES, 4'b0} : 
  (Segment == 3'b001) ? {4'b0, displacement_intermediate} + {CS, 4'b0} : 
  (Segment == 3'b010) ? {4'b0, displacement_intermediate} + {SS, 4'b0} : 
  {4'b0, displacement_intermediate} + {DS, 4'b0};



/*     ereg  reg
000    AX    AL
001    CX    CL
010    DX    DL
011    BX    BL
100    SP    AH
101    BP    CH
110    SI    DH
111    DI    BH

000 ES
001 CS
010 SS
110 DS

*/

always@(*) begin

case(R_M)
	0: index_intermediate = BX + SI;
	1: index_intermediate = BX + DI;
	2: index_intermediate = BP + SI;
	3: index_intermediate = BP + DI;
	4: index_intermediate = SI;
	5: index_intermediate = DI;
	6: index_intermediate = (MOD[0] | MOD[1]) ? BP : {DisplacementHi, DisplacementLo};
	7: index_intermediate = BX;
	default: index_intermediate = BX;
endcase
end

always@(*) begin

case(MOD)
	0: displacement_intermediate = index_intermediate;
	1: displacement_intermediate = index_intermediate + {8'd0, DisplacementLo};
	2: displacement_intermediate = index_intermediate + {DisplacementHi, DisplacementLo};
	3: displacement_intermediate = 16'd0;
endcase 
end


endmodule

 