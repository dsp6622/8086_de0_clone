
vlib work

vlog ALU.v
vlog ALU_tb.v

#start simulation

vsim -L altera_mf_ver -L lpm_ver -L cycloneiii_ver work.ALU_tb

add wave -position end  sim:/ALU_tb/SysClk
add wave -position end  sim:/ALU_tb/Enable
add wave -position end  sim:/ALU_tb/WordByte
add wave -position end -radix hex sim:/ALU_tb/In1
add wave -position end -radix hex sim:/ALU_tb/In2
add wave -position end  sim:/ALU_tb/FLAGS
add wave -position end  sim:/ALU_tb/Op
add wave -position end  sim:/ALU_tb/CAPZSO
add wave -position end -radix hex sim:/ALU_tb/Out

run 1500 ns