`ifndef _cpu_vh_
`define _cpu_vh_


 // Op codes
 // TODO / FIXME: Don't try to group them together into two groups, just group them for example ALURM2R 8'b00xxx01x. 
 // That way escapes aren't needed, the IP can be calculated easily, and the pipeline will be less deep.
// Ignore between below
 `define ALUG1 8'b00xxxxxx
 `define ALUG1R2RM 8'b00xxx00x
 `define ALUG1R2RMb 8'b00xxx000
 `define ALUG1R2RMw 8'b00xxx001
 `define ALUG1RM2R 8'b00xxx01x
 `define ALUG1RM2Rb 8'b00xxx010
 `define ALUG1RM2Rw 8'b00xxx011
 `define ALUG1I2A 8'b00xxx10x
 `define ALUG1I2Ab 8'b00xxx100
 `define ALUG1I2Aw 8'b00xxx101
 `define ALUESC 8'b00xxx11x

// Ignore between above
 `define NOP 8'h90
 `define WRITE_CAPZSO  FLAGS <= {3'b111, CAPZSO[0], FLAGS[10:8], CAPZSO[1], CAPZSO[2], 1'b0, CAPZSO[4], 1'b0, CAPZSO[3], 1'b1, CAPZSO[5]};
 `define ALUR2RM 8'b00xxx0xx
 `define ALUACC 8'b00xxx10x
 `define ALUACCb 8'b00xxx100
 `define ALUACCw 8'b00xxx101

 `define MOVI2R 8'b1011xxxx
 `define MOVB2R 8'b10110xxx
 `define MOVW2R 8'b10111xxx
 `define MOVRM2SEG 8'b10001110
 `define MOVW2M 8'b11000111
 `define MOVB2M 8'b11000110
 `define MOVM2A 8'b1010000x
 `define MOVA2M 8'b1010001x
 `define MOVR2RM 8'b1000100x
 `define MOVR2RMb 8'b10001000
 `define MOVR2RMw 8'b10001001
 `define MOVRM2R 8'b1000101x
 `define MOVRM2Rb 8'b10001010
 `define MOVRM2Rw 8'b10001011
 `define MOVSEG2RM 8'b10001100
 `define PUSHPOPR 8'b0101xxxx
 `define ALUG2 8'b100000xx

//TODO: Handle segment override
 // States
 `define num_states 51
  parameter IDLE = 0,
  INIT      = 1,
  HALT      = 2,
  FETCH1a   = 3,
  FETCH1b   = 4,
  FETCH1c   = 5,
  FETCH2a   = 6,
  FETCH2b   = 7,
  FETCH2c   = 8,
  FETCH3a   = 9,
  FETCH3b   = 10,
  FETCH3c   = 11,
  FETCH4a   = 12,
  FETCH4b   = 13,
  FETCH4c   = 14,
  DECODE    = 15,
  INTHa     = 16,
  MOVR2SEGa = 17, 
  MOVM2SEGa = 18,
  MOVW2Ma   = 19,
  MOVB2Ma   = 20,
  MOVM2Aa   = 21,
  MOVM2Ab   = 22,
  MOVA2Ma   = 23,
  MOVA2Mb   = 24,
  MOVR2Ma   = 25,
  MOVR2Mb   = 26,
  MOVR2Ra   = 27,
  MOVM2Ra   = 28,
  MOVM2Rb   = 29,
  MOVSEG2M  = 30,
  PUSHRa    = 31,
  PUSHRb    = 32,
  PUSHRc    = 33,
  PUSHRd    = 34,
  POPRa     = 35,
  POPRb     = 36,
  POPRc     = 37,
  POPRd     = 38,
  ALURMa    = 39,
  ALURMb    = 40,
  ALURMc    = 41,
  ALURMd    = 42,
  ALURMe    = 43,
  ALUR2Ra   = 44,
  ALUR2Rb   = 45,
  ALUR2Rc   = 46,
  ALUR2Rd   = 47,
  ALUACCa   = 48,
  ALUACCb   = 49,
  ALUACCc   = 50;
  //REMEMBER TO CHANGE NUM STATES
  
  
`define ONE_BYTE_OPS `PUSHPOPR, `NOP
`define TWO_BYTE_OPS `MOVB2R, `ALUACCb
`define THREE_BYTE_OPS `MOVW2R, `MOVM2A, `MOVA2M, `ALUACCw
`define FOUR_BYTE_OPS `MOVRM2SEG, `MOVR2RM, `MOVRM2R, `MOVSEG2RM, `ALUR2RM
`define FIVE_BYTE_OPS `MOVB2M
`define SIX_BYTE_OPS `MOVW2M 
`define MOD_DEPENDENT_OPS `MOVR2RM, `MOVRM2R, `MOVSEG2RM, `ALUR2RM //Also include in maximum size above

`define PREFIXES 8'h2E, 8'h3E, 8'h26, 8'h36
`endif

//There are some op-codes that are extended by [5:3] of the second byte.
//Some of them can be combined since they are similar, but those that can not
//need to be defined below by their op-code and extension.
//`define FF_GROUP `INCM, ...
