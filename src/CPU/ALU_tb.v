
`timescale 1ns/10ps
module ALU_tb();


reg SysClk, Enable, WordByte;
reg[15:0] In1, In2, FLAGS;
reg[2:0] Op;
wire[5:0] CAPZSO;
wire[15:0] Out;
ALU uut(
.SysClk   (SysClk),
.Enable   (Enable),
.WordByte (WordByte),
.In1      (In1),
.In2      (In2),
.Op       (Op),
.FLAGS    (FLAGS),
.Out      (Out),
.CAPZSO   (CAPZSO)
);

integer i;
  initial begin
    SysClk = 1'b0;
    Enable = 1'b1;
    FLAGS = 16'b0;
    forever #10 SysClk = ~SysClk; // generate a clock
  end
   
  initial begin
    #1;
    WordByte = 1'b1;
    for (i = 0; i < 8; i = i + 1) begin
      Op = i;

      In1 = 15'h5555;
      In2 = 15'h5555;
      #20;

      In1 = 15'hFFFF;
      In2 = 15'h5555;
      #20;

      In1 = 15'h0000;
      In2 = 15'h0000;
      #20;

      In1 = 15'h5555;
      In2 = 15'hFFFF;
      #20;
    end
    WordByte = 1'b0;
    for (i = 0; i < 8; i = i + 1) begin
      Op = i;

      In1 = 15'h5555;
      In2 = 15'h5555;
      #20;

      In1 = 15'hFFFF;
      In2 = 15'h5555;
      #20;

      In1 = 15'h0000;
      In2 = 15'h0000;
      #20;

      In1 = 15'h5555;
      In2 = 15'hFFFF;
      #20;
    end
  end
  endmodule