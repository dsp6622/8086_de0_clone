module Top(

input			CLOCK_50,
output [9:0] 	LEDG,
input  			BUTTON0,
input  			BUTTON1,
input  			BUTTON2,
input  [9:0]	SWITCHES,
output wire		GPIO1_D0,
output wire		GPIO1_D1,
output wire    GPIO1_D2,
output wire    GPIO1_D3,

//VGA signals
output [3:0]	VGA_R,
output [3:0]	VGA_G,
output [3:0]	VGA_B,
output 			VGA_HS,
output 			VGA_VS,

//SDRAM
output DRAM_CLK,
output [11:0] DRAM_ADDR,
inout  [15:0] DRAM_DQ,
output [1:0 ] DRAM_BA,
output DRAM_LDQM,
output DRAM_UDQM,
output DRAM_WE_N,
output DRAM_CAS_N,
output DRAM_RAS_N,
output DRAM_CS_N,
output DRAM_CKE,

//PS/2 Keyboard
inout PS2_KBCLK,
inout PS2_KBDAT

);
/*
assign GPIO1_D0 = PS2_KBCLK;
assign GPIO1_D1 = PS2_KBDAT;*/

reg RESET2, RESET1 /* synthesis preserve */ ;
wire SysClk;
wire VGA_CLOCK;

//PLL
VGAPLL pll(
.areset				(1'b0),
.inclk0				(CLOCK_50),
.c0					(VGA_CLOCK),//25MHz
.c1					(SysClk)//100MHz
);
assign DRAM_CLK = SysClk;


wire CPU_DataReady;
wire [15:0] CPU_DataIn;
wire [15:0] CPU_DataOut;
wire [19:0] CPU_AddressOut;
wire WordByte;
wire CPU_WE;
wire CPU_Start;

wire [19:0] BusAddress;
wire [15:0] BusDataIn;
wire BusWE;
wire BusUpperMask;
wire BusLowerMask;
wire BusStartTransaction;
wire [15:0] BusDataOut;
wire BusAck;

wire [14:0] VideoAddress;
wire [15:0] VideoWriteData;
wire VideoWE;
wire VideoStart;
wire VideoUM;
wire VideoLM;
wire [15:0] VideoReadData;
wire VideoAck;

wire [19:0] RamAddress;
wire [15:0] RamWriteData;
wire RamWE;
wire RamStart;
wire RamUM;
wire RamLM;
wire [15:0] RamReadData;
wire RamAck;

wire [12:0] BiosAddress;
wire [15:0] BiosWriteData;
wire BiosWE;
wire BiosStart;
wire BiosUM;
wire BiosLM;
wire [15:0] BiosReadData;
wire BiosAck;

CPU _8086 (
.SysClk (SysClk),
.nReset (RESET2),
.Interrupt (1'b0),
.DataReady (CPU_DataReady),
.InterruptIndex (8'b0),
.DataIn (CPU_DataIn),
.DataOut (CPU_DataOut),
.AddressOut (CPU_AddressOut),
.WordByte (WordByte), //Word = 1, byte = 0
.WriteEnable (CPU_WE),
.StartTransaction (CPU_Start)
);

LITTLE_ENDIAN endianConverter(
.SysClk (SysClk),
.nReset (RESET2),
.CPU_Start (CPU_Start),
.sAddress (CPU_AddressOut),
.sDataIn (CPU_DataOut),
.sWriteEnable (CPU_WE),
.sWordByte (WordByte),
.sDataOut (CPU_DataIn),
.sAck (CPU_DataReady),

.mAddress (BusAddress),
.mDataOut (BusDataIn),
.mWriteEnable (BusWE),
.mUpperMask (BusUpperMask),
.mLowerMask (BusLowerMask),
.mStartTransaction (BusStartTransaction),
.mDataIn (BusDataOut),
.mAck (BusAck)
);

// TODO: Either the video or the SDRAM ack's off by one cycle

CROSSBAR bus(
.Address          (BusAddress),
.DataIn           (BusDataIn),
.WriteEnable      (BusWE),
.UpperMask        (BusUpperMask),
.LowerMask        (BusLowerMask),
.StartTransaction (BusStartTransaction),
.DataOut          (BusDataOut),
.Ack              (BusAck),

.VideoAddress   (VideoAddress),
.VideoWriteData (VideoWriteData),
.VideoWE        (VideoWE),
.VideoStart     (VideoStart),
.VideoUM        (VideoUM),
.VideoLM        (VideoLM),
.VideoReadData  (VideoReadData),
.VideoAck       (VideoAck),

.RamAddress   (RamAddress),
.RamWriteData (RamWriteData),
.RamWE        (RamWE),
.RamStart     (RamStart),
.RamUM        (RamUM),
.RamLM        (RamLM),
.RamReadData  (RamReadData),
.RamAck       (RamAck),

.BiosAddress   (BiosAddress),
.BiosWriteData (BiosWriteData),
.BiosWE        (BiosWE),
.BiosStart     (BiosStart),
.BiosUM        (BiosUM),
.BiosLM        (BiosLM),
.BiosReadData  (BiosReadData),
.BiosAck       (BiosAck)
);




TEXT_VGA video (
.nReset      (RESET2),
.VGAClk      (VGA_CLOCK),
.SysClk      (SysClk),
.Address     (VideoAddress),
.DataIn      (VideoWriteData),
.WriteEnable (VideoWE),
.UpperMask   (VideoUM),
.LowerMask   (VideoLM),
.Start       (VideoStart),
.DataReady   (VideoAck),
.DataOut     (VideoReadData),
.HS_OUT      (VGA_HS),
.VS_OUT      (VGA_VS),
.VGA_R       (VGA_R),
.VGA_G       (VGA_G),
.VGA_B       (VGA_B)
);

BIOS BiosWrap(
.SysClk    (SysClk),
.Address   (BiosAddress),
.WriteData (BiosWriteData),
.WriteEn   (BiosWE),
.Start     (BiosStart),
.UpperMask (BiosUM),
.LowerMask (BiosLM),
.ReadData  (BiosReadData),
.Ack       (BiosAck)
);
//assign LEDG = {data, rdy, SPI_Clk_Out};

//assign GPIO1_D0 = (TestData == 15'b0) ? 1'b1 : 1'b0;
/*
always @(posedge SYS_CLOCK) begin
	if (Ack == 1'b1) begin
	Data <= {TestData[15:11], TestData[3:0]};
	end
end
*/

SDRAM_CTRL sdram(
//Controller logic
.SysClk       (SysClk),				    //System Clock
.DataIn       (RamWriteData),			
.Address      (RamAddress),
.WriteEnable  (RamWE),				    //Write enable
.Start        (RamStart),			   //Start transaction
.niLDQM       (RamUM),
.niUDQM       (RamLM),
.Ack          (RamAck),				    //Set to complete transaction
.DataOut      (RamReadData),			
//SDRAM signals
.SDCLK        (SysClk),						//Clock
.nReset       (RESET2),					//Active low reset
.CLK_EN       (DRAM_CKE),					//Clock enable
.ADDRE        (DRAM_ADDR),						//12 bit address port
.nChip_sel    (DRAM_CS_N),		//Chip select, always high for one chip
.n_RAS        (DRAM_RAS_N),
.n_CAS        (DRAM_CAS_N),
.nLDQM        (DRAM_LDQM),						//Lower data mask
.nUDQM        (DRAM_UDQM),						//Upper data mask
.BA           (DRAM_BA),						//Bank address 
.nWriteEN     (DRAM_WE_N),					
.DQ           (DRAM_DQ)						//SDRAM IO port
);

/*
reg [13:0] counter;
always @(posedge SYS_CLOCK) begin
	 counter <= counter + 1'b1;
    data <= Data;
end
assign SPI_CLK = counter[13];*/
/*
PS2_Controller PS2(
.SYS_CLK	(SYS_CLOCK), //100 MHz
.PS2_CLK	(PS2_KBCLK),
.PS2_DAT	(PS2_KBDAT),
.Err_Out(),
.Int_Out	(inter),
.Parallel_Out (Data),
.Ready_Out (rdy),
.Ack_In (BUTTON1),
.Parallel_In (SWITCHES[7:0]),
.Req_In (~BUTTON2),
.nRESET		(BUTTON0  ),
);
*/

/*
assign GPIO1_D0 = SPI_Clk_Out;
assign GPIO1_D1 = SS;
assign GPIO1_D2 = MISO;
assign GPIO1_D3 = SPI_CLK;
SPI spi(
	.MOSI(MISO),
   .Parallel_Out(Data),
   .Ready(rdy),
   .SPI_Clk_Out(SPI_Clk_Out),
   .SS(SS),
   .MISO(MISO),
   .Parallel_In(SWITCHES[7:0]),
   .SPI_Double_Clk(SPI_CLK),
   .Start_Transaction(~BUTTON1),
   .nReset (BUTTON0)
	);*/

always@(posedge VGA_CLOCK, negedge BUTTON0) begin
    if (~BUTTON0) begin
        RESET2 <= 1'b0;
        RESET1 <= 1'b0;
    end
    else begin
        RESET2 <= RESET1;
        RESET1 <= 1'b1;
    end
end
endmodule
